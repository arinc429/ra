var group__transmitting =
[
    [ "api429_xfer", "structapi429__xfer.html", [
      [ "buf_size", "structapi429__xfer.html#a2c6ac222c1420a8f68a285d7811bdaa0", null ],
      [ "err_type", "structapi429__xfer.html#a2c4307f8923f3db128e8e4f00cfd6470", null ],
      [ "ir_index", "structapi429__xfer.html#acde1f573aef2d37c8c362e2e4cb97018", null ],
      [ "xfer_gap", "structapi429__xfer.html#a5af0e3a5bec1430f2b30e51314c72870", null ],
      [ "xfer_id", "structapi429__xfer.html#a7eb8934a8752973aa9182c403e40f82d", null ],
      [ "xfer_ir", "structapi429__xfer.html#acb11807924c5ccca3e4f4618206c9705", null ],
      [ "xfer_type", "structapi429__xfer.html#a1b0aa4348e66bff5223f9972b0befff6", null ]
    ] ],
    [ "api429_xfer_out", "structapi429__xfer__out.html", [
      [ "ul_FreeMem", "structapi429__xfer__out.html#af479a904825d8ab819abf0f9e60809e7", null ],
      [ "ul_Status", "structapi429__xfer__out.html#ac93f78f19bd2535b15a1d241fa281e5a", null ],
      [ "ul_XferDescAddr", "structapi429__xfer__out.html#a14e8f7981cb578553b440d7445bb5aa5", null ]
    ] ],
    [ "api429_lxfer_dyntag", "structapi429__lxfer__dyntag.html", [
      [ "ul_LowerLimit", "structapi429__lxfer__dyntag.html#ad083b202d80fcd24cbcaed8ef55277e8", null ],
      [ "ul_Mask", "structapi429__lxfer__dyntag.html#a90831a66baf8637f84478ef80539e415", null ],
      [ "ul_Mode", "structapi429__lxfer__dyntag.html#a20301f22d5312f142ea1236d3dedeb32", null ],
      [ "ul_StartValue", "structapi429__lxfer__dyntag.html#a134737a063969c27406b10ad195964d0", null ],
      [ "ul_StepSize", "structapi429__lxfer__dyntag.html#a652547446a070e8e0e5a7ea68e3c1508", null ],
      [ "ul_UpperLimit", "structapi429__lxfer__dyntag.html#afa4ab0e55dacf431825041b8ccbdbbcf", null ]
    ] ],
    [ "api429_xfer_data", "structapi429__xfer__data.html", [
      [ "ul_XferData", "structapi429__xfer__data.html#abf94cfae1655cc215b3b086651d1a755", null ],
      [ "ul_XferTTHigh", "structapi429__xfer__data.html#ab5628b34be6df09ded9eeff3f498bd20", null ],
      [ "ul_XferTTLow", "structapi429__xfer__data.html#a4fd86ba1d3cd5cb8d93b436a81934d08", null ]
    ] ],
    [ "api429_xfer_info", "structapi429__xfer__info.html", [
      [ "ul_XferCnt", "structapi429__xfer__info.html#a9cb45162cd93a58d51a49129e2fb3b41", null ],
      [ "ul_XferData", "structapi429__xfer__info.html#a1a69486075342087aebcbaef86975cc5", null ],
      [ "ul_XferIx", "structapi429__xfer__info.html#a92f5f21f24513bb82d6575814b3faf47", null ],
      [ "ul_XferTTHigh", "structapi429__xfer__info.html#a2ffa61efeee966c1ab548c5e50b6b0ca", null ],
      [ "ul_XferTTLow", "structapi429__xfer__info.html#ad269a67f54d3f59f6dabcab6e21864f4", null ]
    ] ],
    [ "api429_xfer_data_read_input", "structapi429__xfer__data__read__input.html", [
      [ "ul_BufSize", "structapi429__xfer__data__read__input.html#ae49e8e11bfce6e9ee428db65d398a01c", null ],
      [ "ul_BufStart", "structapi429__xfer__data__read__input.html#afdc9c85078c190bd05c6e623972d5a25", null ],
      [ "ul_Mode", "structapi429__xfer__data__read__input.html#a487f706c87e23aa0b1506fc69a1486fd", null ],
      [ "ul_XferId", "structapi429__xfer__data__read__input.html#a986ccc513751ff6e86065e6738f1f366", null ]
    ] ],
    [ "api429_mframe_in", "structapi429__mframe__in.html", [
      [ "pul_Xfers", "structapi429__mframe__in.html#a91e47862e6df1649a604ec379415bd65", null ],
      [ "ul_FrmId", "structapi429__mframe__in.html#a29dc4c55f0d5ba0d96aacf41a5d4b6b4", null ],
      [ "ul_XferCnt", "structapi429__mframe__in.html#a8752b1aa40a615de99abf43c5d9a88c2", null ]
    ] ],
    [ "api429_mframe_out", "structapi429__mframe__out.html", [
      [ "ul_MFrameAddr", "structapi429__mframe__out.html#a469195abe1b73f045f239d6c04fec86c", null ]
    ] ],
    [ "TY_API429_LXFER_DYNTAG", "group__transmitting.html#gaa2598c11e875af7708b091a6cdc56099", null ],
    [ "TY_API429_MFRAME_IN", "group__transmitting.html#ga70a481bc267340288baae28a2393f219", null ],
    [ "TY_API429_MFRAME_OUT", "group__transmitting.html#ga7fdd1f02fc3ee6682bdcd03f560d46de", null ],
    [ "TY_API429_XFER", "group__transmitting.html#ga12f891a1aac327ce169afdfa0fbb5c0a", null ],
    [ "TY_API429_XFER_DATA", "group__transmitting.html#gac4622087ef68abaeb66d53e0b87a16f9", null ],
    [ "TY_API429_XFER_DATA_READ_INPUT", "group__transmitting.html#ga68c33d620ff7d3b990ea571af5f3a7d0", null ],
    [ "TY_API429_XFER_INFO", "group__transmitting.html#ga68e32af4fd04771ff94f34a048ec0617", null ],
    [ "TY_API429_XFER_OUT", "group__transmitting.html#ga95f07664e5dd684f1277604a0f99222c", null ],
    [ "TY_E_API429_TX_MODE", "group__transmitting.html#ga0373d555a50324907714399c361f1636", null ],
    [ "TY_E_API429_XFER_ERROR", "group__transmitting.html#ga8dc6a38ee03002b50c62db2f0d1edff3", null ],
    [ "TY_E_API429_XFER_TYPE", "group__transmitting.html#ga88d60040bd89629fb970abbcddca8dd5", null ],
    [ "api429_tx_mode", "group__transmitting.html#gace099525452d5d4b420fe69c86fb2d12", [
      [ "API429_TX_MODE_FRAMING", "group__transmitting.html#ggace099525452d5d4b420fe69c86fb2d12aa458eeaa23babbb4f48e5743b35496dc", null ],
      [ "API429_TX_MODE_LOOP", "group__transmitting.html#ggace099525452d5d4b420fe69c86fb2d12a9b24d00b91c49f3e22ead10f81f637cf", null ],
      [ "API429_TX_MODE_PHYS_REPLAY", "group__transmitting.html#ggace099525452d5d4b420fe69c86fb2d12a0b43138ce8d602389ca2591ef6a89719", null ],
      [ "API429_TX_MODE_FRAMING_DYNTAG", "group__transmitting.html#ggace099525452d5d4b420fe69c86fb2d12a21621c764b66b8fb15a5e26185701fb8", null ],
      [ "API429_TX_MODE_FIFO", "group__transmitting.html#ggace099525452d5d4b420fe69c86fb2d12a62c79718c2820e10cbbdadd808331737", null ],
      [ "API429_TX_MODE_RATE_CONTROLLED", "group__transmitting.html#ggace099525452d5d4b420fe69c86fb2d12acc1909b26d5e0591ad044fad8abae024", null ]
    ] ],
    [ "api429_xfer_error", "group__transmitting.html#ga6f4f095f036c1e04fae141572c26c936", [
      [ "API429_XFER_ERR_DIS", "group__transmitting.html#gga6f4f095f036c1e04fae141572c26c936a8bf8b6d1a686aae3db05ec0056f425a8", null ],
      [ "API429_XFER_ERR_BCH", "group__transmitting.html#gga6f4f095f036c1e04fae141572c26c936a564b9e817caef24ccc083c8a9ce35639", null ],
      [ "API429_XFER_ERR_BCL", "group__transmitting.html#gga6f4f095f036c1e04fae141572c26c936a22439891c578da6ecaed60a1b91646dc", null ],
      [ "API429_XFER_ERR_COD", "group__transmitting.html#gga6f4f095f036c1e04fae141572c26c936a2421fc66da78e2c993a72036ab72906c", null ],
      [ "API429_XFER_ERR_PAR", "group__transmitting.html#gga6f4f095f036c1e04fae141572c26c936a9602905ef8524902344a54e162a65674", null ],
      [ "API429_XFER_ERR_GAP", "group__transmitting.html#gga6f4f095f036c1e04fae141572c26c936a2df4f44e10faf8e16188d3e79ef0914c", null ],
      [ "API429_XFER_ERR_WAIT", "group__transmitting.html#gga6f4f095f036c1e04fae141572c26c936a257bd8b01cc8040bc5213feeeb04ea1b", null ]
    ] ],
    [ "api429_xfer_type", "group__transmitting.html#ga13150794cd3cecf2f83afa59fc7bdbee", [
      [ "API429_TX_LAB_XFER", "group__transmitting.html#gga13150794cd3cecf2f83afa59fc7bdbeea643384def8ade728e0fd433510459ae4", null ],
      [ "API429_TX_NOP_XFER", "group__transmitting.html#gga13150794cd3cecf2f83afa59fc7bdbeea51257b6941a2f13886836da42e51b2bb", null ],
      [ "API429_TX_STROBE", "group__transmitting.html#gga13150794cd3cecf2f83afa59fc7bdbeea53c13c7f814e29f5c6fa71a1aeca4369", null ],
      [ "API429_TX_DELAY", "group__transmitting.html#gga13150794cd3cecf2f83afa59fc7bdbeea24d138514f037fbf41b12327fcaf01dc", null ],
      [ "API429_TX_LAB_XFER_TT", "group__transmitting.html#gga13150794cd3cecf2f83afa59fc7bdbeeadcfb45e0f60992823c53360bab836704", null ],
      [ "API429_TX_EXT_TRIGGER", "group__transmitting.html#gga13150794cd3cecf2f83afa59fc7bdbeead15aa73fb2f30798b8821abd676f6f83", null ],
      [ "API429_TX_LAB_XFER_32", "group__transmitting.html#gga13150794cd3cecf2f83afa59fc7bdbeea886bb57f9e323cb3ec6e7b0ed863ba5c", null ],
      [ "API429_TX_LAB_XFER_TT_32", "group__transmitting.html#gga13150794cd3cecf2f83afa59fc7bdbeea909dc733e80443ca0755463d28f4b240", null ]
    ] ],
    [ "Api429TxAcycFrameCreate", "group__transmitting.html#gafa7a87e57d795cfb6c81c9e8c8dad3f7", null ],
    [ "Api429TxAcycFrameDelete", "group__transmitting.html#ga6f28a5feb8c80b20324b118ac518b017", null ],
    [ "Api429TxAcycFrameSend", "group__transmitting.html#ga7477840ce904db9aa1660f9c35485230", null ],
    [ "Api429TxAmplitudeSet", "group__transmitting.html#ga5c63471e4c35cf5f1565ab6004782394", null ],
    [ "Api429TxFrameTimeSet", "group__transmitting.html#gad1e7776a282a5f334349b7385faeac16", null ],
    [ "Api429TxInit", "group__transmitting.html#ga0b8c40cccf6ed637113909fd22aaf940", null ],
    [ "Api429TxMajorFrameCreate", "group__transmitting.html#gac0dc02527333324fac78bd1efa87933f", null ],
    [ "Api429TxMajorFrameDelete", "group__transmitting.html#ga17a4f1b05a76401c02aa8aa913834bf1", null ],
    [ "Api429TxMinorFrameCreate", "group__transmitting.html#gad097aefaa6888b5a64dd670f2ed87eaa", null ],
    [ "Api429TxMinorFrameDelete", "group__transmitting.html#gab3bc35364274d0f139b0255ca11fb7c1", null ],
    [ "Api429TxPrepareFraming", "group__transmitting.html#ga7ab3d81d3ff59b123879e8c7ded219b0", null ],
    [ "Api429TxRepetitionCountSet", "group__transmitting.html#gacd06203c5c594c14b71009dc3fce2eca", null ],
    [ "Api429TxStartOnTrigger", "group__transmitting.html#ga45afe2965bcb6d83a9f8b5a00474d644", null ],
    [ "Api429TxStartOnTTag", "group__transmitting.html#ga6a1e5dc120759e270e71fabc09561c90", null ],
    [ "Api429TxStatusGet", "group__transmitting.html#ga260e6afc9688c7c8ae384b27082feb34", null ],
    [ "Api429TxXferBufferOffsetGet", "group__transmitting.html#ga595839d82bd63dd3620f99c152ccf8ea", null ],
    [ "Api429TxXferBufferRead", "group__transmitting.html#ga9699d046a2aa775fd000c618b45f7e2b", null ],
    [ "Api429TxXferBufferWrite", "group__transmitting.html#ga7097ef3ee3ed17899fff1b3380d8c6e6", null ],
    [ "Api429TxXferCreate", "group__transmitting.html#ga968c3270d3f8ad24f862f370957c9f8c", null ],
    [ "Api429TxXferDelete", "group__transmitting.html#ga21fd7a4328755428c8ebb0ef98a724d0", null ],
    [ "Api429TxXferDyntagAssign", "group__transmitting.html#ga69069de01435313170161b446bd0062e", null ],
    [ "Api429TxXferRateAdd", "group__transmitting.html#ga7a3df724f78a1d8cf6436401900fb519", null ],
    [ "Api429TxXferRateRemove", "group__transmitting.html#gafaaf2828771024b65ed60cd4b1c6be5c", null ],
    [ "Api429TxXferRateShow", "group__transmitting.html#gab00a93f8769d1df237a0813e4cfca9a6", null ],
    [ "Api429TxXferSkip", "group__transmitting.html#ga0804c85a41c51d1bf799af468a9cddc5", null ],
    [ "Api429TxXferStatusGet", "group__transmitting.html#ga196a419564e32e75429bf6720342c388", null ]
];