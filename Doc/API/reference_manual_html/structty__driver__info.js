var structty__driver__info =
[
    [ "uc_DeviceGroup", "structty__driver__info.html#a12d21d16d5eef875a9cba2cf64c066b1", null ],
    [ "uc_ReservedB2", "structty__driver__info.html#a76062ada977a856e8285a9cf899441fb", null ],
    [ "uc_ReservedB3", "structty__driver__info.html#acf1d6d9b9d5e7e738101b17f30cf8027", null ],
    [ "uc_ReservedB4", "structty__driver__info.html#a508ac9eb568c22c3832458baaf7cd59b", null ],
    [ "ul_BoardConfig", "structty__driver__info.html#ab1a28edfc82bba8e82ab53b984ac3156", null ],
    [ "ul_BoardType", "structty__driver__info.html#ac9dc8dbeae2ef68eb79a06a3bfee347a", null ],
    [ "ul_DriverFlags", "structty__driver__info.html#a37fff6e6e6d701179b047a6680fbf4da", null ],
    [ "ul_OpenConnections", "structty__driver__info.html#a1bdf1693dde4f9481f00d16c8ab37d2a", null ],
    [ "ul_ReservedLW6", "structty__driver__info.html#a015a7caea45c54c2b7503ce9e73e2d19", null ],
    [ "ul_ReservedLW7", "structty__driver__info.html#af3fd7502ce82ae78d0e0c933be0f79dd", null ],
    [ "ul_ReservedLW8", "structty__driver__info.html#a6d35a075ef9fad688bc9b21a6f5d7c91", null ],
    [ "ul_SN", "structty__driver__info.html#a4eaaa06efab637e815348a0535c5c83f", null ],
    [ "uw_ReservedW1", "structty__driver__info.html#a9d7c3b2644c1e438e380b92c90be6ee1", null ],
    [ "uw_ReservedW2", "structty__driver__info.html#a79334f599f1b4e03c9bc5d4ffb3e1f82", null ]
];