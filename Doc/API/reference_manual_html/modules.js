var modules =
[
    [ "Board Related Functions", "group__board.html", "group__board" ],
    [ "Raw Board Memory Access", "group__board__mem.html", "group__board__mem" ],
    [ "Channel Handling", "group__channel.html", "group__channel" ],
    [ "Discrete Handling", "group__discretes.html", "group__discretes" ],
    [ "Library Administration Functions", "group__lib.html", "group__lib" ],
    [ "Data Replay", "group__replay.html", "group__replay" ],
    [ "Data Monitoring", "group__monitoring.html", "group__monitoring" ],
    [ "Data Receiver", "group__receiving.html", "group__receiving" ],
    [ "Data Transmitting", "group__transmitting.html", "group__transmitting" ],
    [ "FIFO Based Transmitting", "group__txfifo.html", "group__txfifo" ],
    [ "Version Handling", "group__versions.html", "group__versions" ],
    [ "VxWorks Related Functions", "group__vme.html", "group__vme" ]
];