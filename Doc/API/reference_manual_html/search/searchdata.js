var indexSectionsWithContent =
{
  0: "_abcdefghilmnoprstuvx",
  1: "_acdflmtv",
  2: "a",
  3: "abcdefghilmnoprstuvx",
  4: "at",
  5: "a",
  6: "a",
  7: "bcdflrv"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "functions",
  3: "variables",
  4: "typedefs",
  5: "enums",
  6: "enumvalues",
  7: "groups"
};

var indexSectionLabels =
{
  0: "All",
  1: "Data Structures",
  2: "Functions",
  3: "Variables",
  4: "Typedefs",
  5: "Enumerations",
  6: "Enumerator",
  7: "Modules"
};

