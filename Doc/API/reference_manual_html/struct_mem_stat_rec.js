var struct_mem_stat_rec =
[
    [ "maxFreeSize", "struct_mem_stat_rec.html#a7689e9e287b5b01514d3fd595d1cb2b7", null ],
    [ "reserved1", "struct_mem_stat_rec.html#a1464790f3997b37ed6d591a2a46fd6eb", null ],
    [ "reserved2", "struct_mem_stat_rec.html#a207e495ca672e5d7cefd0b7048cc7513", null ],
    [ "totAllocSize", "struct_mem_stat_rec.html#ab4c37dd6ace7fbd721bc23d2a970065d", null ],
    [ "totFreeSize", "struct_mem_stat_rec.html#a10b7eec0fdfb9f8e76e39e7696b3555f", null ],
    [ "totPhysSize", "struct_mem_stat_rec.html#a5a04baa54a3aed1e6ceb3116843c8e09", null ],
    [ "unused1", "struct_mem_stat_rec.html#ac6ca7ed04e9e92716edf830ffa53d017", null ],
    [ "unused2", "struct_mem_stat_rec.html#a73e153e52b09934953d3683a66437a86", null ],
    [ "unused3", "struct_mem_stat_rec.html#afbfac63322ca989f755b64efdcde0e8d", null ],
    [ "unused4", "struct_mem_stat_rec.html#ac120e37af814f7053a39dfb85c9d02df", null ],
    [ "unused5", "struct_mem_stat_rec.html#ac4d09971dc00b05d3dd58033d7dcb4f8", null ]
];