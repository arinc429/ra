var group__discretes =
[
    [ "api429_discr_info", "structapi429__discr__info.html", [
      [ "canIn", "structapi429__discr__info.html#a2204d1576803dd2579e2ddf99643df27", null ],
      [ "canOut", "structapi429__discr__info.html#ac769fbf9302acd817b9791a35603ebf2", null ],
      [ "channels", "structapi429__discr__info.html#a9d54bcf76948fceb127c647615d0597b", null ]
    ] ],
    [ "api429_discrete_pps_config", "structapi429__discrete__pps__config.html", [
      [ "ul_DiscreteChannelId", "structapi429__discrete__pps__config.html#ad407e51b7456e5cc19f4361bf5cf61cd", null ],
      [ "ul_EnaDis", "structapi429__discrete__pps__config.html#afabd66af01945c27b42846a7e8c181c8", null ]
    ] ],
    [ "TY_API429_DISCR_INFO", "group__discretes.html#gab9c86f4ead83e1858778f54f6ef4a3d9", null ],
    [ "TY_API429_DISCRETE_PPS_CONFIG", "group__discretes.html#gad5909442bfee894e0c345674b735d3f2", null ],
    [ "Api429DiscretesConfigGet", "group__discretes.html#gad1d9336f664bcc3419a5e17939ceb1ce", null ],
    [ "Api429DiscretesConfigSet", "group__discretes.html#gac8de1fcffdc55888335849ed5d6d68e3", null ],
    [ "Api429DiscretesInfoGet", "group__discretes.html#ga29b1af3bdfc9638f0d169f1efa5f6cf1", null ],
    [ "Api429DiscretesPPSSet", "group__discretes.html#ga314c14cfbee17cc900e6e5b4cebf01dd", null ],
    [ "Api429DiscretesRead", "group__discretes.html#ga0e32bf07922bb9c479ae97b297f674ec", null ],
    [ "Api429DiscretesWrite", "group__discretes.html#gac6da845e32b60a8be76c86b63bc76a1f", null ]
];