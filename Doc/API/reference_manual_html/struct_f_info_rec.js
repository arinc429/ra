var struct_f_info_rec =
[
    [ "cdate", "struct_f_info_rec.html#a5a504fff59bb37a934ee8c51a547cd56", null ],
    [ "creator", "struct_f_info_rec.html#a4cb9550266f40a4051667b585b513a4d", null ],
    [ "folder", "struct_f_info_rec.html#a9753d001a4c8eb75c5779bb82122c62a", null ],
    [ "group", "struct_f_info_rec.html#ab58d6d72e63aba5eddb5ae264a67dc83", null ],
    [ "h", "struct_f_info_rec.html#a96cec4a452316b1b30fe73a5f23d1591", null ],
    [ "isInvisible", "struct_f_info_rec.html#ac3e46171b5dff9a047f04dabbc784ab5", null ],
    [ "location", "struct_f_info_rec.html#a844d909aa09a09b1bf74399fb297af68", null ],
    [ "mdate", "struct_f_info_rec.html#a922c2a93f79831cfc64bc4fc15130cd9", null ],
    [ "owner", "struct_f_info_rec.html#aa5ac612cc41a93d17621249a3613895d", null ],
    [ "permissions", "struct_f_info_rec.html#a5cafcd1612822607c7ed40c251803bba", null ],
    [ "rfSize", "struct_f_info_rec.html#a851e9c873a7b6ee30a1ffa2f5611d379", null ],
    [ "size", "struct_f_info_rec.html#a9444aa7c4cd83fcc16bdf34f6ed5533b", null ],
    [ "type", "struct_f_info_rec.html#a8a76182901d90480bd0a9e74143673ae", null ],
    [ "v", "struct_f_info_rec.html#a5c0da6ed05c7a57622a87850e62d8a7f", null ]
];