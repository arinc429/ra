var NAVTREE =
[
  [ "Arinc 429 C/C++ based Application Programming Interface", "index.html", [
    [ "Modules", "modules.html", "modules" ],
    [ "Data Structures", "annotated.html", [
      [ "Data Structures", "annotated.html", "annotated_dup" ],
      [ "Data Structure Index", "classes.html", null ],
      [ "Data Fields", "functions.html", [
        [ "All", "functions.html", "functions_dup" ],
        [ "Variables", "functions_vars.html", "functions_vars" ]
      ] ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"annotated.html",
"group__board.html#ggac98485d468a6d0b83b3205e95bc17ce2a477a6e5fc781310501fd14a48d268fbf",
"group__monitoring.html#gga6f2607acd23c57e22f8f82fdb8ef116fa839e3902fe5ed9b421786fdda1e346e6",
"structapi429__channel__info.html#a1740457f7f6951aa1d65095ea907aeda",
"union___t_y___a_y_e___m_s___c_o_n_t_r_o_l.html#a96c1fd16cb73149e3c5653fe24002dec"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';