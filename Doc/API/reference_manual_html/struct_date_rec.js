var struct_date_rec =
[
    [ "hour", "struct_date_rec.html#a6fd5f0e04651a952d0b185b691a72197", null ],
    [ "isdst", "struct_date_rec.html#a4a5d8c488b5e26edcee3ea354c01c31c", null ],
    [ "mday", "struct_date_rec.html#a02586a982c434ce5dbb45c02cb6dcd02", null ],
    [ "min", "struct_date_rec.html#ac9acaaef6ca7e134dac1ad5ab4d383df", null ],
    [ "mon", "struct_date_rec.html#a07367c7277242bf971daef7b350a09c6", null ],
    [ "sec", "struct_date_rec.html#af55e1d644f9589b1bd3043b1ea672b35", null ],
    [ "wday", "struct_date_rec.html#a58e33d247c9a071e00b549e2e1f0c2dc", null ],
    [ "yday", "struct_date_rec.html#a9fdc0e22c241268f7723041530b4caf8", null ],
    [ "year", "struct_date_rec.html#a81cce055d1d2f7670f6e58f397f17643", null ]
];