var group__channel =
[
    [ "api429_channel_info", "structapi429__channel__info.html", [
      [ "active", "structapi429__channel__info.html#a69e2aa158053807b6d707ac475e64521", null ],
      [ "capability_flags", "structapi429__channel__info.html#a54512a62beddd48a7c7c41cef0bcd928", null ],
      [ "config_flags", "structapi429__channel__info.html#a1740457f7f6951aa1d65095ea907aeda", null ],
      [ "speed", "structapi429__channel__info.html#a07c5fafa3f8571776bbbe53d350e848a", null ]
    ] ],
    [ "api429_loglist_entry", "unionapi429__loglist__entry.html", [
      [ "b", "unionapi429__loglist__entry.html#a9545a399505b02dd42c12823a647c671", null ],
      [ "res", "unionapi429__loglist__entry.html#a0dcd3a99b365b78505034e6e3c54cbb6", null ],
      [ "t", "unionapi429__loglist__entry.html#ad86f04406d2ad07886651abf7b631cb2", null ],
      [ "uc_Biu1", "unionapi429__loglist__entry.html#aba40a194427157955a1a8fc8033761da", null ],
      [ "uc_Biu2", "unionapi429__loglist__entry.html#a378b25023a21ce518e802b083133659e", null ],
      [ "uc_Cmd", "unionapi429__loglist__entry.html#a9df0a6f0e4cb1b719abe59134c747644", null ],
      [ "uc_Dma", "unionapi429__loglist__entry.html#a2312977dab0292aa6323a83500ee8a65", null ],
      [ "uc_IntType", "unionapi429__loglist__entry.html#a7f09eb4e1eb3316158f9502368dd0b09", null ],
      [ "uc_Target", "unionapi429__loglist__entry.html#a751cff27148ffe750200226f89270f67", null ],
      [ "ul_All", "unionapi429__loglist__entry.html#a750f4e1b79722b550678f94d8893078e", null ],
      [ "ul_Info", "unionapi429__loglist__entry.html#a3eb78fb1f9227e401d40b13471248b86", null ]
    ] ],
    [ "api429_intr_loglist_entry", "structapi429__intr__loglist__entry.html", [
      [ "ul_Lla", "structapi429__intr__loglist__entry.html#a265372d861341a395d9df21014d90016", null ],
      [ "ul_Llb", "structapi429__intr__loglist__entry.html#a9de59db912d02d5406f38ddea399aa7d", null ],
      [ "ul_Lld", "structapi429__intr__loglist__entry.html#a6149aa4bc42facece6fb7a7a00d714af", null ],
      [ "x_Llc", "structapi429__intr__loglist__entry.html#a9c9e9a525586f3c88e614fddc5b6e1dc", null ]
    ] ],
    [ "API429_CHANNEL_CAN_RECEIVE", "group__channel.html#ga4dfdaef00ffdb0b6ea954baaa4d0e96d", null ],
    [ "API429_CHANNEL_CAN_TRANSMIT", "group__channel.html#ga74421579fd9e3539f03b5d45ea7343d2", null ],
    [ "API429_CHANNEL_HAS_VARIABLE_AMPLITUDE", "group__channel.html#gaf59595ec369450a1647683ca3ae77de1", null ],
    [ "API429_CHANNEL_IN_GLOBAL_MONITORING_MODE", "group__channel.html#gafd35148e1c8746fe560b8ef03fc43331", null ],
    [ "API429_CHANNEL_IN_LOCAL_MONITORING_MODE", "group__channel.html#ga850712ff673f97c0d2040f96babb63b0", null ],
    [ "API429_CHANNEL_IN_MONITORING_MODE", "group__channel.html#ga1a7afd399bb1939e48498b12323d819b", null ],
    [ "API429_CHANNEL_IN_PHYS_REPLAY_MODE", "group__channel.html#gacb5fe3ef5c0eefac9af70767c415faab", null ],
    [ "API429_CHANNEL_IN_REPLAY_MODE", "group__channel.html#ga97014eb5af81059e172338e3f28fc348", null ],
    [ "API429_CHANNEL_IN_RX_LP_MODE", "group__channel.html#ga2a3efea072364087abb77e770ad8d6b1", null ],
    [ "API429_CHANNEL_IN_RX_MODE", "group__channel.html#gae90c1bf3375cb8ea677b03807fd7131a", null ],
    [ "API429_CHANNEL_IN_RX_NORMAL_MODE", "group__channel.html#ga654b7f25b7e32fa3574eaae8c130dc29", null ],
    [ "API429_CHANNEL_IN_RX_TX_MIXING_MODE", "group__channel.html#ga0d318007886bf92c4d8e3e11ede39fb5", null ],
    [ "API429_CHANNEL_IN_TX_DYNTAG_MODE", "group__channel.html#gaab15bb37f98e892d22dc2c82453c66eb", null ],
    [ "API429_CHANNEL_IN_TX_FIFO_MODE", "group__channel.html#ga72cd88d51318f71ac8525bee26db0673", null ],
    [ "API429_CHANNEL_IN_TX_FRAMING_MODE", "group__channel.html#gad901db832014357d6447ca014e5e0578", null ],
    [ "API429_CHANNEL_IN_TX_LP_MODE", "group__channel.html#ga4850011300034720a53b5bcf68c784ff", null ],
    [ "API429_CHANNEL_IN_TX_MODE", "group__channel.html#gad225f13abc4f3ae3b7a5bfb3bb043ab7", null ],
    [ "API429_CHANNEL_IN_TX_RATE_CONTROLLED_MODE", "group__channel.html#ga41a9bbc09a797ac4bac8e8376b600d20", null ],
    [ "API429_CHN_CAP_RX_FLAG", "group__channel.html#ga0b21eef7eaab47c2d4c5e313ffb3ca72", null ],
    [ "API429_CHN_CAP_TX_FLAG", "group__channel.html#ga85cfab20510128aaad3ade38d29d5e68", null ],
    [ "API429_CHN_CAP_VAR_AMP_FLAG", "group__channel.html#gaf766871faf1c55ff91e0c4aada0ad473", null ],
    [ "API429_CHN_CFG_PARITY_ENABLED_FLAG", "group__channel.html#ga951bf395762084b45f54e9c112f3bccd", null ],
    [ "API429_CHN_CFG_PHYS_REPLAY_FLAG", "group__channel.html#ga1c57d3ab595948df1a35d75c3aec12e3", null ],
    [ "API429_CHN_CFG_RM_GLOBAL_FLAG", "group__channel.html#gae30b7d79c06f752718f3d9e5c43c7499", null ],
    [ "API429_CHN_CFG_RM_LOCAL_FLAG", "group__channel.html#ga979fd2fd42821a3bb9b28221884b5b9d", null ],
    [ "API429_CHN_CFG_RX_LABEL_FLAG", "group__channel.html#gaf03dd774384a78cc9015c3d6349362cd", null ],
    [ "API429_CHN_CFG_RX_POLLUTION_FLAG", "group__channel.html#ga34e4b0c9675ec919b1493690228291b7", null ],
    [ "API429_CHN_CFG_SDI_ENABLED_FLAG", "group__channel.html#gae98bfd2f2c4924d56f0ab88c9c9efb45", null ],
    [ "API429_CHN_CFG_TX_DYNTAG_FRAMING_FLAG", "group__channel.html#ga0cafa954bc74ebff8bfe410afc284b0d", null ],
    [ "API429_CHN_CFG_TX_FIFO_FLAG", "group__channel.html#gad7ab3975c2d04ac3e53581a785d3e9f7", null ],
    [ "API429_CHN_CFG_TX_FRAMING_FLAG", "group__channel.html#ga9d2d593c45b6642fa6b51c26643bf0ce", null ],
    [ "API429_CHN_CFG_TX_POLLUTION_FLAG", "group__channel.html#ga29c4bfa804cac5d7f09bda4d752776fd", null ],
    [ "API429_CHN_CFG_TX_RATE_CONTROLLED_FLAG", "group__channel.html#ga6720a01df2aff2f30bda58a2c32fef89", null ],
    [ "API429_CHN_CFG_UNCONFIGURED", "group__channel.html#ga870e545d94703c5343e134e91b80958e", null ],
    [ "API429_CHANNEL_CALLBACK", "group__channel.html#ga4931ed434b8d90b2ade271f7e6d361d4", null ],
    [ "TY_API429_CHANNEL_INFO", "group__channel.html#ga51f5484860fe374a76e2c6a67f964eb4", null ],
    [ "TY_API429_INTR_LOGLIST_ENTRY", "group__channel.html#ga5da256e59b910a3fdb970c53139de1c9", null ],
    [ "TY_API429_LOGLIST_TYPE_ENTRY", "group__channel.html#ga3f044f9f66c92ca2e9689a482d07f35b", null ],
    [ "TY_E_API429_SPEED", "group__channel.html#gae6144c33538555e01657041954229d7e", null ],
    [ "api429_event_type", "group__channel.html#gaccae7a3fa346458784297e15cb73c93a", [
      [ "API429_EVENT_UNDEFINED", "group__channel.html#ggaccae7a3fa346458784297e15cb73c93aa326866a53a545b9805e0555477cb5786", null ],
      [ "API429_EVENT_TX_HALT", "group__channel.html#ggaccae7a3fa346458784297e15cb73c93aafca4ad339f30b0ae6d8420ae85bac3ca", null ],
      [ "API429_EVENT_TX_SKIP", "group__channel.html#ggaccae7a3fa346458784297e15cb73c93aa92ae5a08ad1483f990fb5c4e7d83cff4", null ],
      [ "API429_EVENT_TX_LABEL", "group__channel.html#ggaccae7a3fa346458784297e15cb73c93aa1a0c01ac46e5b2d07d1d6d4b1224f7a3", null ],
      [ "API429_EVENT_TX_INDEX", "group__channel.html#ggaccae7a3fa346458784297e15cb73c93aa396a5dd6f4f37536ffffb5505a0d6290", null ],
      [ "API429_EVENT_RX_ANY_LABEL", "group__channel.html#ggaccae7a3fa346458784297e15cb73c93aadf5ae28ce89e894575269448ebdd5272", null ],
      [ "API429_EVENT_RX_INDEX", "group__channel.html#ggaccae7a3fa346458784297e15cb73c93aa16f6671c24e60f085dafbf5b03680a2c", null ],
      [ "API429_EVENT_RX_ERROR", "group__channel.html#ggaccae7a3fa346458784297e15cb73c93aa4357e9bf630b8122e4834c45e5f4b272", null ],
      [ "API429_EVENT_FUNC_BLOCK", "group__channel.html#ggaccae7a3fa346458784297e15cb73c93aa85c7dfb95b998ef74d89b8ce05175b71", null ],
      [ "API429_EVENT_RM_TRIGGER", "group__channel.html#ggaccae7a3fa346458784297e15cb73c93aa7b7e54083ca2933df419a0b078c923ca", null ],
      [ "API429_EVENT_RM_BUFFER_FULL", "group__channel.html#ggaccae7a3fa346458784297e15cb73c93aaa7568ca874f77fa9a3575dffbc81be9b", null ],
      [ "API429_EVENT_RM_BUFFER_HALF_FULL", "group__channel.html#ggaccae7a3fa346458784297e15cb73c93aa0e34313968cd858f083f8c5a78c528e8", null ],
      [ "API429_EVENT_REPLAY_HALF_BUFFER", "group__channel.html#ggaccae7a3fa346458784297e15cb73c93aa348d397d6cbbb4abc5c0f2df2484af86", null ],
      [ "API429_EVENT_REPLAY_STOP", "group__channel.html#ggaccae7a3fa346458784297e15cb73c93aad430919a62692f37ba0db2a79536c8cc", null ],
      [ "API429_EVENT_TX_FIFO", "group__channel.html#ggaccae7a3fa346458784297e15cb73c93aa41ec70b72f5a1cd6754cefb06b5484d9", null ]
    ] ],
    [ "api429_speed", "group__channel.html#ga384791327969315ca7b28b20045b71a2", [
      [ "API429_LO_SPEED", "group__channel.html#gga384791327969315ca7b28b20045b71a2af15965fccdb9bcc52a3109942370f9ca", null ],
      [ "API429_HI_SPEED", "group__channel.html#gga384791327969315ca7b28b20045b71a2ac607175c7138050fed24fa819b70653e", null ]
    ] ],
    [ "Api429ChannelCallbackRegister", "group__channel.html#gab6bc4e5915f7c35fba77c344adb94521", null ],
    [ "Api429ChannelCallbackUnregister", "group__channel.html#gaedb3e7dc89ce6cf286a3f9eb0eda2efc", null ],
    [ "Api429ChannelClear", "group__channel.html#ga2cc284344229a415206f0d0926494ef0", null ],
    [ "Api429ChannelHalt", "group__channel.html#ga3471730bbb23b4d55e49e9813410cfac", null ],
    [ "Api429ChannelInfoGet", "group__channel.html#gab2addcb7c285c1744d86761734fe1911", null ],
    [ "Api429ChannelSpeedSet", "group__channel.html#gaa28fcc05b2baaaafbd651c9570d43f0a", null ],
    [ "Api429ChannelStart", "group__channel.html#gabe7851fa4a014a24d6146710dcb2fcef", null ]
];