var structty__ver__info =
[
    [ "ac_Description", "structty__ver__info.html#a234585fb8686c149121abb5605883dfd", null ],
    [ "ac_FullVersion", "structty__ver__info.html#a2a5157cf961a812f873aea95bcc0864b", null ],
    [ "ul_BuildNr", "structty__ver__info.html#a136868ebad805a6773293ba62d1300ba", null ],
    [ "ul_MajorVer", "structty__ver__info.html#aa8354b7dce0711a55c68b5216def8d60", null ],
    [ "ul_MinorVer", "structty__ver__info.html#adf2d12a78bf1c5cf936b56ef0d95e51b", null ],
    [ "ul_PatchVersion", "structty__ver__info.html#a03afed4675f65104b175aa632b055100", null ],
    [ "ul_VersionType", "structty__ver__info.html#acd72fad2ba766c32b2f855fa9a01824d", null ]
];