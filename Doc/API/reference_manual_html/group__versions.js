var group__versions =
[
    [ "api429_version", "structapi429__version.html", [
      [ "ul_BuildNr", "structapi429__version.html#a3182b53017b606d1e4f0ddabbf4ac6a2", null ],
      [ "ul_MajorSpecialVer", "structapi429__version.html#a6d7685c1def85d3b9a25ca8a901e0d44", null ],
      [ "ul_MajorVer", "structapi429__version.html#a4ddfde783ee92245013fa36ae5d59357", null ],
      [ "ul_MinorSpecialVer", "structapi429__version.html#a0b8603d5d9a9f585cbb8f5cfc7c6e545", null ],
      [ "ul_MinorVer", "structapi429__version.html#af00fa2e3a5815dee773fe4fa937e3941", null ]
    ] ],
    [ "api429_version_info", "structapi429__version__info.html", [
      [ "ul_BoardSerialNr", "structapi429__version__info.html#a42017f3a843e0ee5ed768514d5037f68", null ],
      [ "x_AslLcaVer", "structapi429__version__info.html#a81ccd463b48956d6f1005dfaca4ace63", null ],
      [ "x_DllVer", "structapi429__version__info.html#a800809083451ce0989d62e0f8e4a6532", null ],
      [ "x_FirmwareBiu1Ver", "structapi429__version__info.html#ad1e96ae503b81137ff93cf1547891dc2", null ],
      [ "x_FirmwareBiu2Ver", "structapi429__version__info.html#abfebdc19550e553679feb3db152a7639", null ],
      [ "x_IoLcaBiu1Ver", "structapi429__version__info.html#a9943b7ef103e25ddc492a91578022151", null ],
      [ "x_IoLcaBiu2Ver", "structapi429__version__info.html#a90ec2c9078b6822968adca6badb30bdc", null ],
      [ "x_MonitorVer", "structapi429__version__info.html#a952ca4dff117b27bdd3bbd8349c69faf", null ],
      [ "x_PciLcaVer", "structapi429__version__info.html#ad7449e8c306af5d3ba59b6c87f0b00be", null ],
      [ "x_SysDrvVer", "structapi429__version__info.html#a69a5d7db51eb4e003242159dd57a05aa", null ],
      [ "x_TargetSWVer", "structapi429__version__info.html#a2bc0b08cfa7d90b7fe7f2065e4b71f2c", null ],
      [ "x_TcpVer", "structapi429__version__info.html#a78732bfd39bde95aa3b945ab4626b4f7", null ],
      [ "x_VmeGeneric", "structapi429__version__info.html#a96e7de402c3a92e51757d4d1b8c60c73", null ]
    ] ],
    [ "TY_API429_VERSION", "group__versions.html#gadd94d0d591fac2a7014593631e89b668", null ],
    [ "TY_API429_VERSION_INFO", "group__versions.html#gae426233d3f94295daad553a2be2f9869", null ],
    [ "Api429ReadBSPVersionEx", "group__versions.html#ga16ae85be217ea1f2e78267a061de5695", null ],
    [ "Api429VersionGet", "group__versions.html#ga7e4a41493e7364b9de2f0b92762d44a3", null ],
    [ "Api429VersionGetAll", "group__versions.html#gafb23d8939f711113890b8e035354d131", null ]
];