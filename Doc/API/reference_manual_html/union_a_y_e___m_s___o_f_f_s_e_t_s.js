var union_a_y_e___m_s___o_f_f_s_e_t_s =
[
    [ "abs_primary_val_30V", "union_a_y_e___m_s___o_f_f_s_e_t_s.html#aa9d6fbed6ebc6f832d73caac97234594", null ],
    [ "abs_primary_val_3V", "union_a_y_e___m_s___o_f_f_s_e_t_s.html#a87761ade93452f0444f759cc8b86dafc", null ],
    [ "abs_secondary_val_30V", "union_a_y_e___m_s___o_f_f_s_e_t_s.html#a49cbebf98b3b2776c3f472a1af12c29f", null ],
    [ "abs_secondary_val_3V", "union_a_y_e___m_s___o_f_f_s_e_t_s.html#a93e8d90baa5b09d4bad45ce1be10f5e0", null ],
    [ "all", "union_a_y_e___m_s___o_f_f_s_e_t_s.html#ae2412779f37d12572614c9006b1fb1e7", null ],
    [ "primary_negative_offset_30V", "union_a_y_e___m_s___o_f_f_s_e_t_s.html#a2327938bc0124527810ce993436860d7", null ],
    [ "primary_negative_offset_3V", "union_a_y_e___m_s___o_f_f_s_e_t_s.html#af85a3386dc9e94f34472f0aac15af6cd", null ],
    [ "reserved1", "union_a_y_e___m_s___o_f_f_s_e_t_s.html#a12833a1b792806ec16f3c1b5ab273a47", null ],
    [ "reserved2", "union_a_y_e___m_s___o_f_f_s_e_t_s.html#abe2012f44b06474e98c3149f1a203baa", null ],
    [ "reserved3", "union_a_y_e___m_s___o_f_f_s_e_t_s.html#a4010d0fe22c851ea2db835dffd633770", null ],
    [ "reserved4", "union_a_y_e___m_s___o_f_f_s_e_t_s.html#abb56f17d0797d7569d8b01f3589c5211", null ],
    [ "secondary_negative_offset_30V", "union_a_y_e___m_s___o_f_f_s_e_t_s.html#a924a73b55f57be5900ac27bd13b4bd80", null ],
    [ "secondary_negative_offset_3V", "union_a_y_e___m_s___o_f_f_s_e_t_s.html#a7c421229a954607330e13a4c14826c9a", null ],
    [ "settings", "union_a_y_e___m_s___o_f_f_s_e_t_s.html#ac7dba82c527b33c29a8e9f53cc942f6e", null ]
];