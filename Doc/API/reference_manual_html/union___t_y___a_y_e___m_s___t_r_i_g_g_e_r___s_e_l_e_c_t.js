var union___t_y___a_y_e___m_s___t_r_i_g_g_e_r___s_e_l_e_c_t =
[
    [ "all", "union___t_y___a_y_e___m_s___t_r_i_g_g_e_r___s_e_l_e_c_t.html#abbd495aba5b88c388804d0658fa1f977", null ],
    [ "external_trigger_source", "union___t_y___a_y_e___m_s___t_r_i_g_g_e_r___s_e_l_e_c_t.html#a396008b4851620aca68e6e158fd3d0a9", null ],
    [ "physical_trigger", "union___t_y___a_y_e___m_s___t_r_i_g_g_e_r___s_e_l_e_c_t.html#a1147aac87fa15af4fcf15ed084166112", null ],
    [ "physical_trigger_mode", "union___t_y___a_y_e___m_s___t_r_i_g_g_e_r___s_e_l_e_c_t.html#a0f3473a9f03970ea82718c5a1e5df709", null ],
    [ "protocol_trigger", "union___t_y___a_y_e___m_s___t_r_i_g_g_e_r___s_e_l_e_c_t.html#a63a62bbc0677a04150e962d575d80ace", null ],
    [ "reserved1", "union___t_y___a_y_e___m_s___t_r_i_g_g_e_r___s_e_l_e_c_t.html#a833a62415283b9c4f8628e3b1ae3e21c", null ],
    [ "reserved2", "union___t_y___a_y_e___m_s___t_r_i_g_g_e_r___s_e_l_e_c_t.html#a548f5fde9f526d76d0ae872264a5c295", null ],
    [ "reserved3", "union___t_y___a_y_e___m_s___t_r_i_g_g_e_r___s_e_l_e_c_t.html#af4a2a198c01a9f1b457d3c003b9f14d0", null ],
    [ "reserved4", "union___t_y___a_y_e___m_s___t_r_i_g_g_e_r___s_e_l_e_c_t.html#acc6b00eb1a06aec0ecf2c3f43aacb4a0", null ],
    [ "settings", "union___t_y___a_y_e___m_s___t_r_i_g_g_e_r___s_e_l_e_c_t.html#a29eeceb8f5e235f4880aa555f8e962f1", null ],
    [ "trigger_source", "union___t_y___a_y_e___m_s___t_r_i_g_g_e_r___s_e_l_e_c_t.html#af23e24b040fbdfe833611ba566b15c25", null ]
];