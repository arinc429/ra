var structapi429__board__info =
[
    [ "capability_flags", "structapi429__board__info.html#ada133cc63e90405a2d51b7545f6d83f9", null ],
    [ "name", "structapi429__board__info.html#afb5e2010fcc56c283376d3e564a9dd35", null ],
    [ "num_channels", "structapi429__board__info.html#a18b95f8538820d1dc147288e2ab39e4c", null ],
    [ "num_trigger_in", "structapi429__board__info.html#a57d681cf57922c884e123d1ab01e46bf", null ],
    [ "num_trigger_out", "structapi429__board__info.html#a956f949b28d4d4725ef59c18ea8847a4", null ],
    [ "serial", "structapi429__board__info.html#a43ab9a4b7842acd13cab67f2270adb75", null ],
    [ "server_url", "structapi429__board__info.html#a75549ab123dc726f43da9cc5f30de954", null ]
];