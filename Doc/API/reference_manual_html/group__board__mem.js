var group__board__mem =
[
    [ "TY_E_API429_MEMORY_OBJECT", "group__board__mem.html#ga53a4fa275fc05c6966cabe5e46436a47", null ],
    [ "api429_memory_object", "group__board__mem.html#ga50ab80e1ffc440410b65214aa4103db5", [
      [ "API429_MEM_OBJ_XFER", "group__board__mem.html#gga50ab80e1ffc440410b65214aa4103db5aa6daf481fa4917133c97233bddb8db36", null ],
      [ "API429_MEM_OBJ_XFER_BUF", "group__board__mem.html#gga50ab80e1ffc440410b65214aa4103db5a51aff9257c6fadf042b1eaece5577744", null ],
      [ "API429_MEM_OBJ_MINFRM", "group__board__mem.html#gga50ab80e1ffc440410b65214aa4103db5ae79857a9368f29edff79775fb9c8748f", null ],
      [ "API429_MEM_OBJ_MAJFRM", "group__board__mem.html#gga50ab80e1ffc440410b65214aa4103db5a532c0d9839f6cf3104f5f269dcf374fd", null ],
      [ "API429_MEM_OBJ_LABDESC", "group__board__mem.html#gga50ab80e1ffc440410b65214aa4103db5aff8dfd5a263946a3081ecf292f02d1d1", null ],
      [ "API429_MEM_OBJ_RXBUF", "group__board__mem.html#gga50ab80e1ffc440410b65214aa4103db5ae3715381e6a8dde1e41802a73d969d33", null ],
      [ "API429_MEM_OBJ_TXCOUNT", "group__board__mem.html#gga50ab80e1ffc440410b65214aa4103db5a60df94d6452dce2a26b3e7ae8b13096b", null ],
      [ "API429_MEM_OBJ_CHN_DESC", "group__board__mem.html#gga50ab80e1ffc440410b65214aa4103db5ab1c7a6145ab35d5bf33600f633c3178b", null ],
      [ "API429_MEM_OBJ_RM_BUF", "group__board__mem.html#gga50ab80e1ffc440410b65214aa4103db5a41d7e01ccd50fe556d06d4f5f52e38d3", null ]
    ] ],
    [ "Api429BoardMemBlockRead", "group__board__mem.html#ga13236f33579cd3c6aec1b3f1a4952995", null ],
    [ "Api429BoardMemBlockWrite", "group__board__mem.html#ga77fee81e810d5d8e3abcf177df688a13", null ],
    [ "Api429BoardMemLocationGet", "group__board__mem.html#gaad70425a72df49cb058f2b59778e9cbf", null ],
    [ "Api429BoardMemRead", "group__board__mem.html#ga4b4a85740c0309260789168de76d50fd", null ],
    [ "Api429BoardMemTimerAddrGet", "group__board__mem.html#ga45dfb408be496e2658d45f694f5479f6", null ],
    [ "Api429BoardMemWrite", "group__board__mem.html#gafad0928efa3d61ba5016e6604949c584", null ]
];