var group__lib =
[
    [ "TY_429PNP_CALLBACK_FUNC_PTR", "group__lib.html#gac22855cea04da0610d8369a9c7d64300", null ],
    [ "TY_E_API429_PNP_EVENT", "group__lib.html#gafc1c0573e9d72bca825b60f9b83aa3b2", null ],
    [ "api429_pnp_event", "group__lib.html#gaaba2dccf73122eaf5238c78b543ec6a1", [
      [ "API429_DEVICE_CONNECTED", "group__lib.html#ggaaba2dccf73122eaf5238c78b543ec6a1a04f3e43fe20a5c8530ec691d4cddd397", null ],
      [ "API429_DEVICE_DISCONNECTED", "group__lib.html#ggaaba2dccf73122eaf5238c78b543ec6a1aab9a0c47d673401862cacbf81312bc13", null ]
    ] ],
    [ "Api429LibDebugLevelSet", "group__lib.html#gaaef1b17fe05e4eb8e9b060122bf5f6d3", null ],
    [ "Api429LibErrorDescGet", "group__lib.html#ga32634f04804b0412878b4e828f6b6573", null ],
    [ "Api429LibExit", "group__lib.html#ga4572c4d6c37a7490699b85c7a9486011", null ],
    [ "Api429LibInit", "group__lib.html#ga1f597adcddb521ae4d71e2ea078e0191", null ],
    [ "Api429LibPnpCallbackSet", "group__lib.html#ga9efd9f129f4d1d5ee8426ac019230556", null ],
    [ "Api429LibServerConnect", "group__lib.html#ga62688f87db6e7a6e2d41bf45a1dd92c4", null ],
    [ "Api429LibServerDisconnect", "group__lib.html#gaf958ee73e214b0d43e2a4437c88a3e45", null ]
];