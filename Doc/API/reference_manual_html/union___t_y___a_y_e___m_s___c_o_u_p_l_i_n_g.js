var union___t_y___a_y_e___m_s___c_o_u_p_l_i_n_g =
[
    [ "all", "union___t_y___a_y_e___m_s___c_o_u_p_l_i_n_g.html#a4a8967586873ef8e3d52a3b395037c93", null ],
    [ "primary_3V", "union___t_y___a_y_e___m_s___c_o_u_p_l_i_n_g.html#ad5b6dda2ff15dfdab58ebd2655641175", null ],
    [ "primary_to_signal_source", "union___t_y___a_y_e___m_s___c_o_u_p_l_i_n_g.html#a5054744c18dd9d69d055011b1ef41e88", null ],
    [ "primary_to_stub", "union___t_y___a_y_e___m_s___c_o_u_p_l_i_n_g.html#a7e9a28fee886c9b97a49dab7e5fe373b", null ],
    [ "reserved", "union___t_y___a_y_e___m_s___c_o_u_p_l_i_n_g.html#ae216f11f59d0af6bf869bf6b10687a26", null ],
    [ "reserved2", "union___t_y___a_y_e___m_s___c_o_u_p_l_i_n_g.html#a4a04399cddf671d6a21fc9cf46413b14", null ],
    [ "secondary_3V", "union___t_y___a_y_e___m_s___c_o_u_p_l_i_n_g.html#a2c203b50ef34a0bab7d59315ee64cc68", null ],
    [ "secondary_to_signal_source", "union___t_y___a_y_e___m_s___c_o_u_p_l_i_n_g.html#a9b8b47980f70a044d340c605d491a71d", null ],
    [ "secondary_to_stub", "union___t_y___a_y_e___m_s___c_o_u_p_l_i_n_g.html#a4c36e083e81d91a13edebbfa9aaa88ba", null ],
    [ "settings", "union___t_y___a_y_e___m_s___c_o_u_p_l_i_n_g.html#ac7a8d9e3baa765edc288f72471576231", null ]
];