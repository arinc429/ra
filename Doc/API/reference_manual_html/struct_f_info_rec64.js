var struct_f_info_rec64 =
[
    [ "cdate", "struct_f_info_rec64.html#ae645277bffc010e2cb3fee3525e09b89", null ],
    [ "creator", "struct_f_info_rec64.html#ab56dc0662cd59369f6a272411473253d", null ],
    [ "folder", "struct_f_info_rec64.html#a12cd954c94e824df36c4f6376d59c9a9", null ],
    [ "group", "struct_f_info_rec64.html#a2868153c376e247eb73866dc3b958456", null ],
    [ "h", "struct_f_info_rec64.html#aeba775d2d032ab0128866eba6347ba40", null ],
    [ "isInvisible", "struct_f_info_rec64.html#af7a2af09f00cee4e19ac51dac1d3f123", null ],
    [ "location", "struct_f_info_rec64.html#aebdbc9f1e431b7cf634f8ca4d7c91e19", null ],
    [ "mdate", "struct_f_info_rec64.html#abda086834bee3953163583869de4cf4a", null ],
    [ "owner", "struct_f_info_rec64.html#a9daf7e8791d61146799b0e75e27d7ea8", null ],
    [ "permissions", "struct_f_info_rec64.html#a7bea8cb51e3d4b291a580f0eff8ac3f2", null ],
    [ "rfSize", "struct_f_info_rec64.html#a95833704e28264b67c2eb3fd8cd463bd", null ],
    [ "size", "struct_f_info_rec64.html#ab6f76a076877a1c5c06db4cb5fe89c6c", null ],
    [ "type", "struct_f_info_rec64.html#a042f508ce0601ebcde288bebdb1f9780", null ],
    [ "v", "struct_f_info_rec64.html#a04923457f0ff1ce725a57d3599f693f5", null ]
];