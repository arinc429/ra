var group__txfifo =
[
    [ "api429_tx_fifo_setup", "structapi429__tx__fifo__setup.html", [
      [ "ulDefaultGapSize", "structapi429__tx__fifo__setup.html#a5d80e72132658c9e750f32b0ea50fa21", null ],
      [ "ulFifoControl", "structapi429__tx__fifo__setup.html#acccd45d02fb83ffeca9d0910980f475e", null ],
      [ "ulFifoSize", "structapi429__tx__fifo__setup.html#aa873ffc0bde88874e3550f113bde85e9", null ]
    ] ],
    [ "api429_tx_fifo_status", "structapi429__tx__fifo__status.html", [
      [ "ulEntriesFree", "structapi429__tx__fifo__status.html#a308301822756adb9ee5581c6a4231799", null ],
      [ "ulEntriesToSend", "structapi429__tx__fifo__status.html#a40c31063ba8caddf25126016a80f078e", null ]
    ] ],
    [ "api429_tx_fifo_entry", "structapi429__tx__fifo__entry.html", [
      [ "ulControl", "structapi429__tx__fifo__entry.html#abd999210ac21782884d329e9476159cd", null ],
      [ "ulData", "structapi429__tx__fifo__entry.html#a168ecffa42a9e6f344760176a34f683b", null ]
    ] ],
    [ "TY_API429_TX_FIFO_ENTRY", "group__txfifo.html#ga521b12a9e6c7c5cc7a11f6494d5dbaab", null ],
    [ "TY_API429_TX_FIFO_SETUP", "group__txfifo.html#ga214576861390afa77072e106fb01d4cb", null ],
    [ "TY_API429_TX_FIFO_STATUS", "group__txfifo.html#ga9d94605dc1b2fe52972bce8e46860a27", null ],
    [ "Api429TxFifoDataWordCreate", "group__txfifo.html#ga6c11edaa677e6bd4a07cb533288d84e0", null ],
    [ "Api429TxFifoDataWordsWrite", "group__txfifo.html#gaad5a3e0b2e17efae12081ebccaa1f276", null ],
    [ "Api429TxFifoDelayCreate", "group__txfifo.html#ga6cdca3bb4c937d28585290ce62f0c76c", null ],
    [ "Api429TxFifoInterruptCreate", "group__txfifo.html#ga314196521124c749d4a67facefc92cac", null ],
    [ "Api429TxFifoReset", "group__txfifo.html#gaccdec167ce8cc8a1d540a2eb40ec7536", null ],
    [ "Api429TxFifoSetup", "group__txfifo.html#ga404a0b6a6d823a97d47d85dccd637a5c", null ],
    [ "Api429TxFifoSetupGet", "group__txfifo.html#ga542f43526f554b8537f2381e4a5b60d6", null ],
    [ "Api429TxFifoStatusGet", "group__txfifo.html#ga9c6d669899db7ad19ca6b76ee78ab79f", null ],
    [ "Api429TxFifoTriggerPulseCreate", "group__txfifo.html#ga6fcd8fd0d4ee384ad2b5482f040f134a", null ],
    [ "Api429TxFifoTriggerWaitCreate", "group__txfifo.html#gaf6e6766dc4bc7dc23a2ec18f3e2e892b", null ],
    [ "Api429TxFifoWrite", "group__txfifo.html#ga49530fbd846f87c7cffac88ccdfae10e", null ]
];