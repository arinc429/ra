var union___ay_e_dma_entry_control_word =
[
    [ "all", "union___ay_e_dma_entry_control_word.html#ab35dc5da7475f237e27e8c9120f8717f", null ],
    [ "bUsePattern", "union___ay_e_dma_entry_control_word.html#a9cde79089b2aceeafa10430003a344b1", null ],
    [ "DmaDest", "union___ay_e_dma_entry_control_word.html#a014a21ce16001cfb28b245233547a033", null ],
    [ "DmaSource", "union___ay_e_dma_entry_control_word.html#a16b808f0802fa27fd64296cca31145f1", null ],
    [ "Interrupt", "union___ay_e_dma_entry_control_word.html#ae43d19308ce14b3b141c0fb01be2c68a", null ],
    [ "MilScopeBlock", "union___ay_e_dma_entry_control_word.html#a5693ae7d68edf18646291b211df27656", null ],
    [ "part", "union___ay_e_dma_entry_control_word.html#a5bd2c807664191c735c8050465e23a45", null ],
    [ "Size", "union___ay_e_dma_entry_control_word.html#a51c0a74d128079e9d25281fac490c920", null ],
    [ "StopDma", "union___ay_e_dma_entry_control_word.html#a12dfc7d2cea6835a8734da57d8c44812", null ]
];