/* SPDX-FileCopyrightText: 2009-2022 AIM GmbH <info@aim-online.com> */
/* SPDX-License-Identifier: MIT */

#ifndef __429_SAMPLE_H__
#define __429_SAMPLE_H__

#include "AiOs.h"
#include "Api429.h"
#include "A429_sample_utils.h"


/* header files with system dependent information (for example for device mapping) */
#if defined(MVME5100)
  #include "map/MVME5100.h"
#elif defined(MV6100)
  #include "map/MVME6100.h"
#elif defined(CUSTOM)
  #include "map/CUSTOM.h"
#elif defined(_AIM_XENOMAI)
  #include "os/xenomai/initialize.h"
#else
  #define sample_map_board(x)
  #define sample_unmap_board(x)
#endif

#define _AIM_WINDOWS




/* functions implemented in function files */
AiReturn PrintBoardVersions(   AiUInt8 boardHandle);
AiReturn TemplateSample(       AiUInt8 boardHandle);
AiReturn RxSample(             AiUInt8 boardHandle, AiUInt8 channelId);
AiReturn RxMonitorSample(      AiUInt8 boardHandle, AiUInt8 channelId);
AiReturn TxFramingSample(      AiUInt8 boardHandle, AiUInt8 channelId);
AiReturn TxInterruptSample(    AiUInt8 boardHandle, AiUInt8 channelId);
AiReturn RxInterruptSample(    AiUInt8 boardHandle, AiUInt8 channelId);
AiReturn TxAcyclicSample(      AiUInt8 boardHandle, AiUInt8 channelId);
AiReturn TxRateOrientedSample( AiUInt8 boardHandle, AiUInt8 channelId);
AiReturn TxDyntagSample(       AiUInt8 boardHandle, AiUInt8 channelId);
AiReturn TxBlockReloadReplay(  AiUInt8 boardHandle, AiUInt8 channelId);
AiReturn TxFifoBasicSample(    AiUInt8 boardHandle, AiUInt8 channelId);
AiReturn TxFifoInterruptSample(AiUInt8 boardHandle, AiUInt8 channelId);
AiReturn RxTxLoopSample(       AiUInt8 boardHandle, AiUInt8 channelId);
AiReturn TxPhysicalReplaySample(AiUInt8 boardHandle, AiUInt8 channelId);


#endif /* __429_SAMPLE_H__ */







