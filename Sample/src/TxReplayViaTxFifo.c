/* SPDX-FileCopyrightText: 2001-2021 AIM GmbH <info@aim-online.com> */
/* SPDX-License-Identifier: MIT */

#include "A429_sample.h"

#define RP_FILE_NAME "k:/replay.dat"





AiReturn getTimeDiff(union api429_tm_tag *time1, union api429_tm_tag *time2, AiUInt32 *microSecDiff)
{
	AiUInt32 seconds1, seconds2, secondsDiff;

	seconds1 = time1->b.minutes * 60 + time1->b.seconds;
	seconds2 = time2->b.minutes * 60 + time2->b.seconds;

	secondsDiff = seconds2 - seconds1;

	*microSecDiff = ((secondsDiff * 1000000) + (time2->b.microseconds - time1->b.microseconds));

	return 0;
}


/* Send four Arinc429 data word write entries and two interrupt event
* entries to the TX FIFO
*/
static AiReturn WriteEntriesToFifo(AiUInt8 boardHandle, AiUInt8 channelId, struct api429_rcv_stack_entry* mon_entries, AiUInt32 count)
{
	AiReturn retVal;

	AiUInt32 i;

	union api429_tm_tag time1;
	union api429_tm_tag time2;
	AiUInt32 microSecDiff;

	AiUInt32 ulEntriesWritten;
	struct api429_tx_fifo_entry *fifo_input;

	enum api429_xfer_error err;

	fifo_input = (struct api429_tx_fifo_entry *) malloc(2 * count * sizeof(struct api429_tx_fifo_entry));



	time1.all = mon_entries[0].tm_tag.all;
	time2.all = time1.all;

	for (i = 0; i < count; i++)
	{
		time1.all = mon_entries[i].tm_tag.all;

		getTimeDiff(&time2, &time1, &microSecDiff);

		time2.all = time1.all;

		printf("%08X %08X %08x:  ", mon_entries[i].ldata, mon_entries[i].brw.all, mon_entries[i].tm_tag.all);
		printf("%02d:%02d:%02d.%06d   ", mon_entries[i].brw.b.hours, mon_entries[i].tm_tag.b.minutes, mon_entries[i].tm_tag.b.seconds, mon_entries[i].tm_tag.b.microseconds);

		printf("err: %d, delta: %d\n", mon_entries[i].brw.b.e_type, microSecDiff);

		switch (mon_entries[i].brw.b.e_type)
		{
		default:
		case 0 /* 0b00000 */: err = API429_XFER_ERR_DIS; break;
		case 3 /* 0b00011 */: err = API429_XFER_ERR_BCH; break;
		case 5 /* 0b00101 */: err = API429_XFER_ERR_COD; break;
		case 9 /* 0b01001 */: err = API429_XFER_ERR_GAP; break;
		case 17/* 0b10001 */: err = API429_XFER_ERR_PAR; break;
		}

		retVal = Api429TxFifoDelayCreate(&fifo_input[2 * i], microSecDiff / 100);
		AIM_CHECK_RESULT_RETURN(retVal);

		retVal = Api429TxFifoDataWordCreate(&fifo_input[(2 * i) + 1], mon_entries[i].ldata, 4, err);
		AIM_CHECK_RESULT_RETURN(retVal);

	}


	retVal = Api429TxFifoWrite(boardHandle, channelId, count * 2, fifo_input, AiTrue, &ulEntriesWritten);
	AIM_CHECK_RESULT_RETURN(retVal);

	free(fifo_input);

	return retVal;
}

/* ######################################################################################*/
/*                                                                                       */
/*                                     Sample entry point                                */
/*                                                                                       */
/* ######################################################################################*/
AiReturn TxReplayViaTxFifoSample(AiUInt8 boardHandle, AiUInt8 channelId)
{
    AiReturn retVal = 0;

    FILE *px_ReplayFile;
    AiUInt32 ul_FileSize = 0;
    AiUInt32 ul_fileGotSize = 0;
    void    *p_DataBuffer = NULL;

	AiUInt32 ul_BlockSize;
	AiUInt32 entries_to_send = 0;

	char cKey;

	/* Api429TxStatusGet */
	AiUInt8 b_TxStatus;
	AiUInt32 l_GlbCnt;





    printf("Running TxReplayViaTxFifoSample \r\n");

    AiOsGetFileSize(RP_FILE_NAME, &ul_FileSize);


	retVal = Api429TxInit(boardHandle, channelId, API429_TX_MODE_FIFO, 0);
	AIM_CHECK_RESULT_RETURN(retVal);

	retVal = Api429ChannelStart(boardHandle, channelId);
    AIM_CHECK_RESULT_RETURN(retVal);



	ul_BlockSize = 100 * sizeof(struct api429_rcv_stack_entry);
	p_DataBuffer = malloc(ul_BlockSize);

	if (NULL == p_DataBuffer)
	{
		printf("Unable to allocate buffer\r\n");
		return 1;
	}



	// open Replay File
	printf("\n Open File %s to Replay\n", RP_FILE_NAME);
	px_ReplayFile = fopen(RP_FILE_NAME, "rb");
	if (px_ReplayFile == NULL)
		return 1;


	ul_fileGotSize = fread(p_DataBuffer, 1 /*Byte*/, ul_BlockSize, px_ReplayFile);


	entries_to_send += ul_fileGotSize / sizeof(struct api429_rcv_stack_entry);
	WriteEntriesToFifo(boardHandle, channelId, (struct api429_rcv_stack_entry*) p_DataBuffer, ul_fileGotSize / sizeof(struct api429_rcv_stack_entry));


	retVal = Api429ChannelStart(boardHandle, channelId);
	AIM_CHECK_RESULT_RETURN(retVal);
	printf("\n");

	printf("Press 'q' to quit or 'any key' to print the TX counter\r\n");
	cKey = 0;
	while (cKey != 'q'){
		ul_fileGotSize = fread(p_DataBuffer, 1 /*Byte*/, ul_BlockSize, px_ReplayFile);

		entries_to_send += ul_fileGotSize / sizeof(struct api429_rcv_stack_entry);
		WriteEntriesToFifo(boardHandle, channelId, (struct api429_rcv_stack_entry*) p_DataBuffer, ul_fileGotSize / sizeof(struct api429_rcv_stack_entry));


		retVal = Api429TxStatusGet(boardHandle, channelId, &b_TxStatus, &l_GlbCnt);
		AIM_CHECK_RESULT_RETURN(retVal);

		printf("\rApi429TxStatusGet:  %d/%d Transfers sent", l_GlbCnt, entries_to_send);

		cKey = AiOsGetChar();
	}
	printf("\r\n");


    /* Clean up */
    if (NULL != p_DataBuffer)
        free(p_DataBuffer);

    fclose(px_ReplayFile);



    return retVal;
}
