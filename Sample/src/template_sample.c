/* SPDX-FileCopyrightText: 2009-2021 AIM GmbH <info@aim-online.com> */
/* SPDX-License-Identifier: MIT */

#include "A429_sample.h"

/* ######################################################################################*/
/*                                                                                       */
/*                                     Sample entry point                                */
/*                                                                                       */
/* ######################################################################################*/
AiReturn TemplateSample( AiUInt8 boardHandle )
{
    AiReturn retVal = 0;

    printf("Running TemplateSample (board %d) \r\n", boardHandle);

    return retVal;
}
