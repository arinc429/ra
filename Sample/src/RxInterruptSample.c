/* SPDX-FileCopyrightText: 2001-2021 AIM GmbH <info@aim-online.com> */
/* SPDX-License-Identifier: MIT */

/******************************************************************************
 *  Includes
******************************************************************************/
#include "A429_sample.h"


/******************************************************************************
 *  Defines
******************************************************************************/


/******************************************************************************
 *  local Vars (Modul)
 *  For easier use within interrupt context
******************************************************************************/
static AiUInt32 interruptCount = 0;

/* struct to hold all information from an interrupt 
   so that we can print it at the end of the sample */
struct sample_int_info{
    AiUInt8 boardHandle;
    AiUInt8 type;
    AiUInt8 channelId;
    AiUInt32 ul_Lla;
    AiUInt32 ul_Llb;
    AiUInt32 ul_Llc;
    AiUInt32 ul_Lld;
    
    AiUInt32 index;
    AiUInt32 dataword;
};

static struct sample_int_info x_Int_Info[20];




/************************************
      Some Makros
************************************/


/******************************************************************************
 *  local prototypes
******************************************************************************/
static AiReturn SetupRx( AiUInt8 boardHandle, AiUInt8 channelId );
static void AI_CALL_CONV userInterruptFunction(AiUInt8 boardHandle, AiUInt8 channelId, enum api429_event_type type, struct api429_intr_loglist_entry* x_Info);


/******************************************************************************
 ******************************************************************************
 **
 ** Sample
 **
 ******************************************************************************
 ******************************************************************************/

AiReturn RxInterruptSample(AiUInt8 boardHandle, AiUInt8 channelId)
{
    AiReturn retVal = 0;
    AiUInt16 i;
    char cKey;

    AiUInt8 ucRxStatus;
    AiUInt32 ulMsgCnt = 0;
    AiUInt32 ulErrCnt = 0;

    printf("Running RX Interrupt Sample for channel %d \r\n", channelId);

    /* Now init the interrupt counter*/
    interruptCount = 0;

    printf("Now install the Interrupt Handler Functions \r\n");
    retVal = Api429ChannelCallbackRegister(boardHandle, channelId, 0, userInterruptFunction);
    AIM_CHECK_RESULT_RETURN(retVal);

    SetupRx(boardHandle, channelId);

    retVal = Api429ChannelStart(boardHandle, channelId);
    AIM_CHECK_RESULT(retVal);

    /* Show the ammount of interrupts raised */
    printf("Press 'q' to quit and show details of some interrupts or 'any key' to print the interrupt counter\r\n");
    do
    {
        /* Read how many transfer were received */
        /* This is optional. Maybe you want to comment it out, to see what happens. */
        retVal = Api429RxStatusGet(boardHandle, channelId, &ucRxStatus, &ulMsgCnt, &ulErrCnt);
        AIM_CHECK_RESULT(retVal);

        /* Show the global variable "interruptCount", which is updated in function "userInterruptFunction" */
        printf("\r Xfers: %d+%d    Intr: %d  ", ulMsgCnt, ulErrCnt, interruptCount);

        cKey = AiOsGetChar();
    } while (cKey != 'q');
    printf("\r\n\n");

    printf("\n\n\rBrd  Type Chn  Lla      Llb      Llc      Lld         Buffer  /index\n");
    for (i = 0; i < 20; i++) {

        printf("%04X %04X %04X %08X %08X %08X %08X   %08X /%2d\n",
            x_Int_Info[i].boardHandle,
            x_Int_Info[i].type,
            x_Int_Info[i].channelId,
            x_Int_Info[i].ul_Lla,
            x_Int_Info[i].ul_Llb,
            x_Int_Info[i].ul_Llc,
            x_Int_Info[i].ul_Lld,
            x_Int_Info[i].dataword,
            x_Int_Info[i].index);
    }

    /* clean up */

    retVal = Api429ChannelHalt(boardHandle, channelId);
    AIM_CHECK_RESULT(retVal);

    printf("\r\nNow uninstall the Interrupt Handler Functions \r\n");
    retVal = Api429ChannelCallbackUnregister(boardHandle, channelId, 0);
    AIM_CHECK_RESULT(retVal);

    return retVal;
} /* end: RxInterruptSample */

static AiReturn SetupLabelWithInterrupt(AiUInt8 boardHandle, AiUInt8 channelId, AiUInt32 labelId)
{
    AiReturn  retVal = 0;

    /* Api429RxLabelConfigure */
    AiUInt8   uc_Status;
    AiUInt32  ul_FreeMem;

    /* Api429RxLabelBufferWrite */
    struct api429_rx_buf_entry	ax_LData[1];
    struct api429_rx_label_setup  label_setup;


    label_setup.label = labelId;
    label_setup.sdi = 0;
    label_setup.con = AiTrue;
    label_setup.irCon = 0x7; /* enable all interrupt sources */
    label_setup.irIndex = 1;
    label_setup.bufSize = 1;
    retVal = Api429RxLabelConfigure(boardHandle, channelId, &label_setup, &uc_Status, &ul_FreeMem);
    AIM_CHECK_RESULT_RETURN(retVal);

    if (0 != uc_Status)
    {
        printf("  Api429RxLabelConfigure was unable to allocate a label buffer\r\n");
        printf("  Not enough memory on board. Status: %i   free memory: 0x%x\r\n", uc_Status, ul_FreeMem);
        return 1;
    }

    /* the given array is used for initialization of the label receive buffers */
    ax_LData[0].lab_data = 0x12345678L;

    /* calling this function is not mandatory, but can be used to initialize to initialize
    * the RX buffer e.g. with some easy recognizable values or with zero */
    retVal = Api429RxLabelBufferWrite(boardHandle, channelId,
                                      (AiUInt8)labelId,
                                      0,                   /* SDI not used here */
                                      (AiUInt16)label_setup.bufSize,
                                      ax_LData,            /* initialization data */
                                      AiFalse);
    AIM_CHECK_RESULT_RETURN(retVal)

    return retVal;
}


static AiReturn SetupRx(AiUInt8 boardHandle, AiUInt8 channelId)
{
    AiReturn  retVal = 0;
    AiUInt32  ul_LabIndex;


    printf("Setting up Rx channel\r\n");

    /*retVal = Api429ChannelSpeedSet( boardHandle, channelId, API429_HI_SPEED ); // calling this function is not mandatory, use default instead */

    /* Intialize the receiver to normal mode and use MSB as parity bit */
    retVal = Api429RxInit(boardHandle, channelId, AiFalse, AiFalse);
    AIM_CHECK_RESULT_RETURN(retVal);

    /* loop over all labels 0..255 = 256 possible labels */
    for (ul_LabIndex = 0; ul_LabIndex <= 255; ul_LabIndex++)
    {
        retVal = SetupLabelWithInterrupt(boardHandle, channelId, ul_LabIndex);
        AIM_CHECK_RESULT_RETURN(retVal);
    }

    return retVal;
}



/******************************************************************************
 **
 ** Sample: Rx Interrupt handler function
 **         Please refer to function Api429ChannelCallbackRegister() in the reference 
 **         manual for details of the user interrupt function
 **
 ******************************************************************************/
static void AI_CALL_CONV userInterruptFunction(AiUInt8 boardHandle, AiUInt8 channelId, enum api429_event_type type, struct api429_intr_loglist_entry* x_Info)
{
    AiUInt32 aul_Data[1 + 1];
    AiUInt32 ul_DataBufferBaseAddr;
    AiUInt32 ul_BytesRead;
    AiReturn retVal;

    /* initialize important data */
    aul_Data[0] = 0;
    aul_Data[1] = 0;

    /* Note: Do not use printf commands within the interrupt context
             Use e.g. sprintf instead and print the buffer at a later point */
    /* Note: for VxWorks, do not use commands whose name starts with Api429Cmd within the interrupt
             context, as they use a semaphore which does not work in the interrupt context.
             If you resume a VxWorks task here, you are free to use all commands in this task
             For Linux and Windows these restrictions do not apply. */

    if ((type == API429_EVENT_RX_ANY_LABEL) ||
        (type == API429_EVENT_RX_ERROR) ||
        (type == API429_EVENT_RX_INDEX) )
    {
        ul_DataBufferBaseAddr = x_Info->ul_Lld & 0x00FFFFFF;

        /* read buffer contents plus buffer header word
         * in this sample only 1+1 entries are read, as we have a buffer size of 1 */
        retVal = Api429BoardMemBlockRead(boardHandle, AI_MEMTYPE_GLOBAL, ul_DataBufferBaseAddr, 4, (void *)aul_Data, 1 + 1, &ul_BytesRead);

        /* aul_Data[0] is the buffer header */
        /* aul_Data[1] and following contain the transmitted data */
    }

    /* Sample: copy interrupt information so that we can print it out later */
    if (interruptCount < 20)
    {
        x_Int_Info[interruptCount].boardHandle = boardHandle;
        x_Int_Info[interruptCount].type = type;
        x_Int_Info[interruptCount].channelId = channelId;
        x_Int_Info[interruptCount].ul_Lla = x_Info->ul_Lla;
        x_Int_Info[interruptCount].ul_Llb = x_Info->ul_Llb;
        x_Int_Info[interruptCount].ul_Llc = x_Info->x_Llc.ul_All;
        x_Int_Info[interruptCount].ul_Lld = x_Info->ul_Lld;
        x_Int_Info[interruptCount].index = aul_Data[0] & 0x000003FF; /* may be interesting for buffers > 1 */
        x_Int_Info[interruptCount].dataword = aul_Data[1];
    }
    interruptCount++;
}

/* EOF */
