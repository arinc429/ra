/* SPDX-FileCopyrightText: 2001-2021 AIM GmbH <info@aim-online.com> */
/* SPDX-License-Identifier: MIT */

/******************************************************************************
 *  Includes
******************************************************************************/
#include "A429_sample.h"


/******************************************************************************
 *  Defines
******************************************************************************/


/******************************************************************************
 *  local Vars (Modul)
 *  For easier use within interrupt context
******************************************************************************/
static AiUInt32 interruptCount = 0;

/* struct to hold all information from an interrupt 
   so that we can print it at the end of the sample */
struct sample_int_info{
    AiUInt8 boardHandle;
    AiUInt8 type;
    AiUInt8 channelId;
    AiUInt32 ul_Lla;
    AiUInt32 ul_Llb;
    AiUInt32 ul_Llc;
    AiUInt32 ul_Lld;
    
    AiUInt32 index;
    AiUInt32 dataword;
};

static struct sample_int_info x_Int_Info[20];



/************************************
      Some Makros
************************************/


/******************************************************************************
 *  local prototypes
******************************************************************************/
static AiReturn PrepareTxSetupInt(AiUInt8 boardHandle, AiUInt8 channelId);
static void AI_CALL_CONV userInterruptFunction(AiUInt8 boardHandle, AiUInt8 channelId, enum api429_event_type type, struct api429_intr_loglist_entry* x_Info);


/******************************************************************************
 ******************************************************************************
 **
 ** Sample
 **
 ******************************************************************************
 ******************************************************************************/

AiReturn TxInterruptSample(AiUInt8 boardHandle, AiUInt8 channelId)
{
    AiReturn retVal;
    int i;
    char cKey;

    printf("Running TX Interrupt Sample for channel %d \r\n", channelId);

    /* Now init the interrupt counter*/
    interruptCount = 0;

    printf("Now install the Interrupt Handler Functions \r\n");
    retVal = Api429ChannelCallbackRegister(boardHandle, channelId, 0, userInterruptFunction);
    AIM_CHECK_RESULT_RETURN(retVal);

    retVal = PrepareTxSetupInt(boardHandle, channelId);
    AIM_CHECK_RESULT_RETURN(retVal);

    /* Start the channel */
    retVal = Api429ChannelStart(boardHandle, channelId);
    AIM_CHECK_RESULT_RETURN(retVal);
    printf("\n");

    /* Show the ammount of interrupts raised */
    printf("Press 'q' to quit and show details of some interrupts or 'any key' to print the interrupt counter\r\n");
    do {
        printf("\r Intr Board1: %d  ", interruptCount);

        cKey = AiOsGetChar();
    } while (cKey != 'q');

    printf("\r\n\n");


    printf("\n\rBrd  Type Chn  Lla      Llb      Llc      Lld         Buffer  /index\n");
    for (i = 0; i < 20; i++) {

        printf("%04X %04X %04X %08X %08X %08X %08X   %08X /%2d\n",
            x_Int_Info[i].boardHandle,
            x_Int_Info[i].type,
            x_Int_Info[i].channelId,
            x_Int_Info[i].ul_Lla,
            x_Int_Info[i].ul_Llb,
            x_Int_Info[i].ul_Llc,
            x_Int_Info[i].ul_Lld,
            x_Int_Info[i].dataword,
            x_Int_Info[i].index);
    }

    /* clean up */

    retVal = Api429ChannelHalt(boardHandle, channelId);
    AIM_CHECK_RESULT_RETURN(retVal);

    printf("\r\nNow uninstall the Interrupt Handler Functions \r\n");
    retVal = Api429ChannelCallbackUnregister(boardHandle, channelId, 0);
    AIM_CHECK_RESULT_RETURN(retVal);

    printf("\r Intr Board1: %d  ", interruptCount);

    return 0;
} /* end: TxInterruptSample */



static AiReturn SetupTransferWithInterruptsAndRate(AiUInt8 boardHandle, AiUInt8 channelId, AiUInt32 transferId, AiUInt32 data[4], AiUInt32 rate)
{
    AiReturn retVal;

    /* Api429CmdTxXferDef */
    struct api429_xfer x_Xfer;
    struct api429_xfer_out x_XferInfo;


    memset(&x_Xfer, 0, sizeof(x_Xfer));
    x_Xfer.xfer_id = transferId;
    x_Xfer.xfer_type = API429_TX_LAB_XFER;
    x_Xfer.xfer_ir = 1; /* Enable transfer interrupt (bit zero is set) */
    x_Xfer.buf_size = 4;
    x_Xfer.xfer_gap = 4;
    retVal = Api429TxXferCreate(boardHandle, channelId, &x_Xfer, &x_XferInfo);
    AIM_CHECK_RESULT_RETURN(retVal);

    if ((x_XferInfo.ul_Status != 0) || (x_XferInfo.ul_FreeMem == 0))
        printf("  Api429CmdTxXferDef was unable to allocate memory\r\n");

    /* Setting the data to be sent */
    retVal = Api429TxXferBufferWrite(boardHandle, channelId, x_Xfer.xfer_id, 1, x_Xfer.buf_size, data);
    AIM_CHECK_RESULT_RETURN(retVal);

    /* Set the rate */
    retVal = Api429TxXferRateAdd(boardHandle, channelId, transferId, rate);
    AIM_CHECK_RESULT_RETURN(retVal);

    return retVal;
}

AiReturn PrepareTxSetupInt(AiUInt8 boardHandle, AiUInt8 channelId)
{
    AiReturn retVal = 0;
    AiUInt32 ul_Index;

    AiUInt32  aul_XferLWord[4];


    printf("\r\nPreparing TX channel\r\n");

    retVal = Api429TxInit(boardHandle, channelId, API429_TX_MODE_RATE_CONTROLLED, 0);
    AIM_CHECK_RESULT_RETURN(retVal);


    /*retVal = Api429CmdTxAmpIni( boardHandle, channelId, 149 ); // calling this function is not mandatory, use default instead */

    /*retVal = Api429ChannelSpeedSet( boardHandle, channelId, API429_HI_SPEED ); // calling this function is not mandatory, use default instead */



    /* Setting the data to be sent */
    for (ul_Index = 0; ul_Index < 4; ul_Index++)
        aul_XferLWord[ul_Index] = 0x0000F000 | (ul_Index + 1);

    /* Setting up a transfer */
    retVal = SetupTransferWithInterruptsAndRate(boardHandle, channelId, 1, aul_XferLWord, 10);
    AIM_CHECK_RESULT_RETURN(retVal);


    return retVal;
}

/******************************************************************************
 **
 ** Sample: Tx Interrupt handler function
 **         Please refer to function Api429ChannelCallbackRegister() in the reference 
 **         manual for details of the user interrupt function
 **
 ******************************************************************************/
static void AI_CALL_CONV userInterruptFunction(AiUInt8 boardHandle, AiUInt8 channelId, enum api429_event_type type, struct api429_intr_loglist_entry* x_Info)
{
  AiUInt32 aul_Data[4+1];
  AiUInt32 ul_DataBufferBaseAddr;
  AiUInt32 ul_BytesRead;
  AiReturn retVal;

  /* initialize important data */
  aul_Data[0] = 0;

  /* Note: Do not use printf commands within the interrupt context
           Use e.g. sprintf instead and print the buffer at a later point */
  /* Note: for VxWorks, do not use commands whose name starts with Api429Cmd within the interrupt 
           context, as they use a semaphore which does not work in the interrupt context. 
           If you resume a VxWorks task here, you are free to use all commands in this task
           For Linux and Windows these restrictions do not apply. */

  if( type == API429_EVENT_TX_LABEL )
  {
    ul_DataBufferBaseAddr = x_Info->ul_Lld & 0x00FFFFFF;

    /* read buffer contents plus buffer header word
     * in this sample only 4+1 entries are read, as we have a buffer size of 4. */
    retVal = Api429BoardMemBlockRead( boardHandle, AI_MEMTYPE_GLOBAL, ul_DataBufferBaseAddr, 4, (void *)aul_Data, 4+1, &ul_BytesRead );

    /* aul_Data[0] is the buffer header */
    /* aul_Data[1] and following contain the transmitted data */
  }

  
  /* Sample: copy interrupt information so that we can print it out later */
  if(interruptCount<20){
    x_Int_Info[interruptCount].boardHandle = boardHandle;
    x_Int_Info[interruptCount].type        = type;
    x_Int_Info[interruptCount].channelId   = channelId;
    x_Int_Info[interruptCount].ul_Lla      = x_Info->ul_Lla;
    x_Int_Info[interruptCount].ul_Llb      = x_Info->ul_Llb;
    x_Int_Info[interruptCount].ul_Llc      = x_Info->x_Llc.ul_All;
    x_Int_Info[interruptCount].ul_Lld      = x_Info->ul_Lld;
    x_Int_Info[interruptCount].index       = aul_Data[0] & 0x000003FF;

    /* the interrupt is raised after the data was sent or received.
     * So we have to read the data before */
    if(x_Int_Info[interruptCount].index == 4)
      x_Int_Info[interruptCount].index = 0;
    x_Int_Info[interruptCount].index++;

    x_Int_Info[interruptCount].dataword = aul_Data[ x_Int_Info[interruptCount].index ];
  }
  interruptCount++;
}

/* EOF */
