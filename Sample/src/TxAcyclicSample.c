/* SPDX-FileCopyrightText: 2001-2021 AIM GmbH <info@aim-online.com> */
/* SPDX-License-Identifier: MIT */

/******************************************************************************

    Description:

    Send some transfers without using any framing.
    This can be done in addition to the normal framing, but this sample shows it
    without doing anything in the normal framing.

******************************************************************************/


/******************************************************************************
 *  Includes
******************************************************************************/
#include "A429_sample.h"


/******************************************************************************
 *  Defines
******************************************************************************/
#define XFER_ID_OF_ACYCLIC  2

/******************************************************************************
 *  local Vars (Modul)
******************************************************************************/

/******************************************************************************
 *  extern Vars
******************************************************************************/


/************************************
      Some Makros
************************************/


/******************************************************************************
 *  global variables used only within this file
******************************************************************************/


/******************************************************************************
 *  local prototypes
******************************************************************************/
AiReturn SetupTxMajorFrame( AiUInt8 boardHandle, AiUInt8 channelId );
AiReturn SetupTxAcyclic(AiUInt8 boardHandle, AiUInt8 channelId, AiUInt32 *pul_AcycFrameId);
AiReturn SendData(AiUInt8 boardHandle, AiUInt8 channelId, AiUInt32 ul_Data, AiUInt32 ul_AcycFrameId);


/******************************************************************************
 ******************************************************************************
 **
 ** Sample
 **
 ******************************************************************************
 ******************************************************************************/

AiReturn TxAcyclicSample(AiUInt8 boardHandle, AiUInt8 channelId)
{
    AiReturn retVal;
    char cKey;
    AiUInt32 ul_AcycFrameId;

    /* Api429TxStatusGet */
    AiUInt8 b_TxStatus;
    AiUInt32 l_GlbCnt;

    printf("Running TX Acyclic Sample for channel %d \r\n", channelId);

    /* Setup a TX channel */
    retVal = SetupTxMajorFrame(boardHandle, channelId);
    AIM_CHECK_RESULT_RETURN(retVal);
    retVal = SetupTxAcyclic(boardHandle, channelId, &ul_AcycFrameId);
    AIM_CHECK_RESULT_RETURN(retVal);

    /* Start the channel */
    retVal = Api429ChannelStart(boardHandle, channelId);
    AIM_CHECK_RESULT_RETURN(retVal);
    printf("\n");

    printf("Press 'q' to quit, 's' to send or 'any key' to print the TX counter\r\n");
    cKey = 0;
    while (cKey != 'q')
    {
        if (cKey == 's')
            SendData(boardHandle, channelId, 0x00000101, ul_AcycFrameId);

        retVal = Api429TxStatusGet(boardHandle, channelId, &b_TxStatus, &l_GlbCnt);
        AIM_CHECK_RESULT_RETURN(retVal);

        printf("\rApi429TxStatusGet:  %d Transfers sent", l_GlbCnt);

        cKey = AiOsGetChar();
    }
    printf("\r\n");

    /* Clean up */
    retVal = Api429ChannelHalt(boardHandle, channelId);
    AIM_CHECK_RESULT_RETURN(retVal);

    return 0;
} /* end: main */



/*************************************************************
 *
 * Setup TX channel, so that doesn't send data, when activated
 *
 *************************************************************/
AiReturn SetupTxMajorFrame( AiUInt8 boardHandle, AiUInt8 channelId )
{
	AiReturn retVal;

	retVal = Api429TxInit( boardHandle, channelId, API429_TX_MODE_RATE_CONTROLLED, 0 ); /* in this mode an empty major frame is automatically created */
	AIM_CHECK_RESULT_RETURN(retVal);

	/* Instead of calling Api429TxAmplitudeSet, the default value of 9.0V (line to line) is used */

	/* Instead of calling Api429ChannelSpeedSet, the default value HIGH SPEED is used. */

	return retVal;
}

/*************************************************************
 *
 * Prepare acyclic transmission
 *
 *************************************************************/
AiReturn SetupTxAcyclic(AiUInt8 boardHandle, AiUInt8 channelId, AiUInt32 *pul_AcycFrameId)
{
    AiReturn retVal = 0;

    /* Api429CmdTxXferDef */
    struct api429_xfer x_Xfer;
    struct api429_xfer_out x_XferInfo;

    /* Api429CmdTxAcycFrameInst */
    AiUInt32  aul_Xfers[1];


    /* Setup Acyclic Transfer and frame.*/
    memset(&x_Xfer, 0, sizeof(x_Xfer));
    x_Xfer.xfer_id = XFER_ID_OF_ACYCLIC;
    x_Xfer.xfer_type = API429_TX_LAB_XFER;
    x_Xfer.buf_size = 1;
    retVal = Api429TxXferCreate(boardHandle, channelId, &x_Xfer, &x_XferInfo);
    AIM_CHECK_RESULT_RETURN(retVal);

    if ((x_XferInfo.ul_Status != 0) || (x_XferInfo.ul_FreeMem == 0))
        printf("  Api429CmdTxXferDef was unable to allocate memory\r\n");
    aul_Xfers[0] = x_Xfer.xfer_id;

    retVal = Api429TxAcycFrameCreate(boardHandle, channelId, 1/*ulXferCnt*/, &aul_Xfers[0], pul_AcycFrameId);
    AIM_CHECK_RESULT_RETURN(retVal);

    return retVal;
}


/*************************************************************
 *
 * Send one specific ARINC 429 data word
 *
 *************************************************************/

AiReturn SendData(AiUInt8 boardHandle, AiUInt8 channelId, AiUInt32 ul_Dataword, AiUInt32 ul_AcycFrameId)
{
    AiReturn retVal = 0;

    retVal = Api429TxXferBufferWrite(boardHandle, channelId, XFER_ID_OF_ACYCLIC, 1, 1/*buffer size*/, &ul_Dataword);
    AIM_CHECK_RESULT_RETURN(retVal);

    retVal = Api429TxAcycFrameSend(boardHandle, channelId, ul_AcycFrameId);
    AIM_CHECK_RESULT_RETURN(retVal);

    return retVal;
}

/* EOF */
