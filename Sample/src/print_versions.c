/* SPDX-FileCopyrightText: 2009-2021 AIM GmbH <info@aim-online.com> */
/* SPDX-License-Identifier: MIT */

#include "A429_sample.h"


AiReturn PrintBoardVersions(AiUInt8 boardHandle)
{
    struct api429_version_info x_VersionInfo;
    struct ty_ver_info* versions = NULL;
    struct ty_version_out version_result = { 0 };
    AiUInt32 i = 0;

    AiReturn               retVal = 0;
    AiUInt8                ucStatus           = 0;
    AiUInt8                ucEcho             = 0;

    memset( &x_VersionInfo, 0, sizeof(x_VersionInfo) );

    printf("Running PrintBoardVersions \r\n");

    /* -- Check how many versions there may be available and allocate
     *--- an array with enough space--- */
    retVal = Api429VersionGetAll(boardHandle, 0, NULL, &version_result);
    AIM_CHECK_RESULT_RETURN(retVal);

    versions = malloc(sizeof(struct ty_ver_info) * version_result.ul_MaxCount);
    if(!versions)
    {
        printf( "  Failed to allocate memory for version read\r\n");
        return 1;
    }

    /* Now read all versions of the board */
    retVal = Api429VersionGetAll(boardHandle, version_result.ul_MaxCount, versions, &version_result);
    AIM_CHECK_RESULT_RETURN(retVal);

    printf("\r\n");
    printf("Version info : \r\n");

    for(i = 0; i < version_result.ul_Count; i++)
    {
        printf("    %-16s:    %-64s\r\n", versions[i].ac_Description, versions[i].ac_FullVersion);
    }

    printf("\r\n");

    free(versions);

    /* -- Execute onboard selftest --- */
    printf("\r\nExecuting selftest ... ");
    retVal = Api429BoardSelftest( boardHandle, 0, &ucStatus, &ucEcho );
    AIM_CHECK_RESULT_RETURN(retVal);

    if( ucStatus != 0 )
        printf(" FAILED %2d/%2d \r\n", ucStatus, ucEcho );
    else
        printf(" PASSED %2d/%2d \r\n", ucStatus, ucEcho );

    return 0;
}

/* EOF */
