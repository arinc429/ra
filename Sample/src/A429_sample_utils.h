/* SPDX-FileCopyrightText: 2016-2021 AIM GmbH <info@aim-online.com> */
/* SPDX-License-Identifier: MIT */

/******************************************************************************

Description:                                                                 
ARINC 429 helper functions

******************************************************************************/

#define AIM_CHECK_RESULT( x ) if( x != 0 ){ \
                                    fprintf( stderr, "\r\n"); \
                                    fprintf(stderr, "ERROR: %s (%d)\r\n", Api429LibErrorDescGet(x), x); \
                                    fprintf( stderr, "%s:%d\r\n", __FILE__, __LINE__ ); \
                                    fprintf( stderr, "\r\n"); \
                              };

#define AIM_CHECK_RESULT_RETURN( x ) if( x != 0 ){ \
                                            AIM_CHECK_RESULT( x ); \
                                            return x; \
                                     };

