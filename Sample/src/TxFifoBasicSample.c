/* SPDX-FileCopyrightText: 2009-2021 AIM GmbH <info@aim-online.com> */
/* SPDX-License-Identifier: MIT */

#include "A429_sample.h"

#define NUMBER_OF_LABELS  10


/* ######################################################################################*/
/*                                                                                       */
/*                                     Sample entry point                                */
/*                                                                                       */
/* ######################################################################################*/
AiReturn TxFifoBasicSample(AiUInt8 boardHandle, AiUInt8 channelId)
{
    AiReturn retVal;
    char cKey;
    int i;
    AiUInt32 expectedTxCount = 0;

    /* Api429TxStatusGet */
    AiUInt8 b_TxStatus;
    AiUInt32 l_GlbCnt;

    /* Api429CmdTxFifoWriteLabels */
    AiUInt32 aulLabels[NUMBER_OF_LABELS];
    AiUInt32 ulEntriesWritten;


    printf("Running TX FIFO Sample for channel %d \r\n", channelId);


    /**************************************/
    /* Prepare channel for fifo           */
    /**************************************/

    /* Enable TX FIFO mode */
    retVal = Api429TxInit(boardHandle, channelId, API429_TX_MODE_FIFO, 0);
    AIM_CHECK_RESULT_RETURN(retVal);


    /* Start the TX channel */
    retVal = Api429ChannelStart(boardHandle, channelId);
    AIM_CHECK_RESULT_RETURN(retVal);
    printf("\n");

    AiUInt32 ul_Mask = 0xFF;
    AiUInt32 ul_Value = 0xFF;
    retVal = Api429DiscretesWrite(boardHandle, ul_Mask,ul_Value);
    AIM_CHECK_RESULT_RETURN(retVal);
    printf("\n");

    /***************************************/
    /* Show status and fill FIFO on demand */
    /***************************************/

    printf("Press 'q' to quit, 's' to send or any other key to print the TX counter\r\n");
    cKey = 0;
    while (cKey != 'q'){

        if (cKey == 's')
        {
            /* put new data into the FIFO */
            for (i = 0; i < NUMBER_OF_LABELS; i++)
                aulLabels[i] = (i * 0x100) + 1;
            retVal = Api429TxFifoDataWordsWrite(boardHandle, channelId, NUMBER_OF_LABELS, aulLabels, AiFalse, &ulEntriesWritten);
            AIM_CHECK_RESULT_RETURN(retVal);

            expectedTxCount += NUMBER_OF_LABELS;
        }


        retVal = Api429TxStatusGet(boardHandle, channelId, &b_TxStatus, &l_GlbCnt);
        AIM_CHECK_RESULT_RETURN(retVal);

        printf("\rApi429TxStatusGet:  %d of %d Transfers sent", l_GlbCnt, expectedTxCount);

        cKey = AiOsGetChar();
    }
    printf("\r\n");


    /**************************************/
    /* clean up                           */
    /**************************************/
    retVal = Api429ChannelHalt(boardHandle, channelId);
    AIM_CHECK_RESULT_RETURN(retVal);

    retVal = Api429TxStatusGet(boardHandle, channelId, &b_TxStatus, &l_GlbCnt);
    printf("\rApi429TxStatusGet:  %d of %d Transfers sent", l_GlbCnt, expectedTxCount);

    return retVal;
}
