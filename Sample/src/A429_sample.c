/* SPDX-FileCopyrightText: 2009-2021 AIM GmbH <info@aim-online.com> */
/* SPDX-License-Identifier: MIT */

#include "A429_sample.h"



/* Datatypes used in sample.c */
typedef struct ConfigStruct
{
    enum api429_board_id ulDevice;
    AiUInt8 channel;
    AiUInt8 test;
    AiUInt8 useServer;
    AiChar  acServer[28];
} ConfigStruct;

/* help functions in A429_sample.c */
void PrintSampleSetup(AiUInt8 boardHandle, AiUInt8 channelId);
void ReadConfiguration(int argc, char *argv[], ConfigStruct * pxConfig);
void WaitForUser();


int __cdecl main( int argc, char *argv[ ] )
{
    ConfigStruct xConfig;
    AiInt16 wBoardCountRemote = 0;
    AiUInt8 boardHandle = 0;
    AiReturn retVal = 0;
    AiUChar sample_name[40];


    printf("\nSample for AIM A429 boards\n");
    printf("======================================================\n");
    printf("This program is licensed under the MIT license.\n");
    printf("Copyright (c) 2009-2021 AIM GmbH <info@aim-online.com>\n\n");
    printf("======================================================\n");

    ReadConfiguration(argc, argv, &xConfig);


    /* -- 1.) Open the API library --- */

    /* map the board 
     * This is specific to the CPU and the VxWorks BSP in use.
     * Some examples for mapping are added to Sample/src folder. In case one of these CPUs are
     * being used, they can be enabled with the appropriate define in the sample project
     */
    sprintf(sample_name, "sample_mod_%d_chn_%d", xConfig.ulDevice, xConfig.channel);
    sample_map_board(sample_name);
    

    /* Local board access */
    Api429LibInit();

    if( xConfig.useServer )
    {
        /* Remote board access */
        retVal = Api429LibServerConnect(xConfig.acServer, &wBoardCountRemote);
        AIM_CHECK_RESULT_RETURN(retVal);
    }


    /* -- 2.) Open the board --- */

    retVal = Api429BoardOpen(xConfig.ulDevice, xConfig.acServer, &boardHandle);
    AIM_CHECK_RESULT_RETURN(retVal);


    /* ######################################################################################*/
    /*                                                                                       */
    /*                                                                                       */
    /* ######################################################################################*/

    PrintSampleSetup(boardHandle, xConfig.channel);

    switch( xConfig.test )
    {
        case  0 : retVal = TemplateSample(          boardHandle); break;
        case  1 : retVal = PrintBoardVersions(      boardHandle); break;
        case  2 : retVal = RxSample(                boardHandle, xConfig.channel); break;
        case  3 : retVal = RxMonitorSample(         boardHandle, xConfig.channel); break;
        case  4 : retVal = TxFramingSample(         boardHandle, xConfig.channel); break;
        case  5 : retVal = TxInterruptSample(       boardHandle, xConfig.channel); break;
        case  6 : retVal = RxInterruptSample(       boardHandle, xConfig.channel); break;
        case  7 : retVal = TxAcyclicSample(         boardHandle, xConfig.channel); break;
        case  8 : retVal = TxRateOrientedSample(    boardHandle, xConfig.channel); break;
        case  9 : retVal = TxDyntagSample(          boardHandle, xConfig.channel); break;
        case 10 : retVal = TxFifoBasicSample(       boardHandle, xConfig.channel); break;
        case 11 : retVal = TxFifoInterruptSample(   boardHandle, xConfig.channel); break;
        case 12 : retVal = RxTxLoopSample(          boardHandle, xConfig.channel); break;
        case 13 : retVal = TxPhysicalReplaySample(  boardHandle, xConfig.channel); break;

        default :
            printf("Sample %d not found\r\n", xConfig.test );
            break;
    }
    if (retVal) printf("\r\nSample returned %d\r\n", retVal);


    /* ######################################################################################*/
    /*                                                                                       */
    /*                                                                                       */
    /* ######################################################################################*/

    /* Clear the used channel.
     * It will be stopped and all used resources are freed
     */
    retVal = Api429ChannelClear(boardHandle, xConfig.channel);
    AIM_CHECK_RESULT(retVal);

    retVal = Api429BoardClose(boardHandle);
    AIM_CHECK_RESULT(retVal);

    if (xConfig.useServer)
    {
        retVal = Api429LibServerDisconnect(xConfig.acServer);
        AIM_CHECK_RESULT(retVal);
    }

    retVal = Api429LibExit();
    AIM_CHECK_RESULT(retVal);

    sample_unmap_board(sample_name);

    WaitForUser();
    return 0;
}



void PrintSampleList()
{
    printf("\r\n");
    printf("Samples :\r\n");

    printf("-- All Boards ---- \r\n"             );
    printf("  0  TemplateSample\r\n"             );
    printf("  1  PrintBoardVersions\r\n"         );
    printf("  2  RxSample\r\n"                   );
    printf("  3  RxMonitorSample\r\n"            );
    printf("  4  TxFramingSample\r\n"            );
    printf("  5  TxInterruptSample\r\n"          );
    printf("  6  RxInterruptSample\r\n"          );
    printf("  7  TxAcyclicSample\r\n"            );
    printf("  8  TxRateOrientedSample\r\n"       );
    printf("  9  TxDyntagSample\r\n"             );
    printf(" 10  TxFifoBasicSample\r\n"          );
    printf(" 11  TxFifoInterruptSample\r\n"      );
    printf(" 12  RxTxLoopSample\r\n"             );
    printf(" 13  TxPhysicalReplaySample\r\n"     );

    printf("\r\n");
}

void ReadConfiguration( int argc, char *argv[ ], ConfigStruct * pxConfig  )
{
    char buf[255];
    int retval;
    int tmp;

    if( argc == 5 )
    {
        /* use command line arguments */
        sscanf(argv[1], "%s", pxConfig->acServer  );
        sscanf(argv[2], "%d", (int*) &pxConfig->ulDevice );
        sscanf(argv[3], "%d", &tmp);  pxConfig->channel = (AiUInt8)tmp;
        sscanf(argv[4], "%d", &tmp);  pxConfig->test = (AiUInt8)tmp;

        if (AiOsStringCmp(pxConfig->acServer, "local") != 0)
            pxConfig->useServer = 0;
        else
            pxConfig->useServer = 1;
    }
    else if( argc == 2 )
    {
        /* only one argument, print usage */
        printf("%s\r\n",                                      argv[0] );
        printf("%s help\r\n",                                 argv[0] );
        printf("%s server module_nr stream_nr sample_nr\r\n", argv[0] );

        PrintSampleList();

        WaitForUser();
    }
    else
    {
        printf("server name (Default=\"local\") ? " );
        AiOsGetString(pxConfig->acServer, sizeof(pxConfig->acServer), "local");
        if (AiOsStringCmp(pxConfig->acServer, "local") != 0)
            pxConfig->useServer = 0;
        else
            pxConfig->useServer = 1;

        printf("module number [0..x] (Default=0) ? " );
        AiOsGetString( buf, sizeof(buf), "0");
        retval = sscanf(buf, "%d", (int*) &pxConfig->ulDevice );
        if(retval < 1)
        {
            fprintf(stderr, "Invalid module ID. Using default\n");
            pxConfig->ulDevice = API429_BOARD_1;
        }

        printf("channel number [1..x] (Default=1) ? " );
        AiOsGetString( buf, sizeof(buf), "1");
        retval = sscanf(buf, "%d", &tmp); pxConfig->channel = (AiUInt8)tmp;
        if(retval < 1)
        {
            fprintf(stderr, "Invalid channel ID. Using default\n");
            pxConfig->channel = 1;
        }

        PrintSampleList();

        printf("sample number (Default=0) ? " );
        AiOsGetString( buf, sizeof(buf), "0");
        retval = sscanf(buf, "%d", &tmp); pxConfig->test = (AiUInt8)tmp;
        if(retval < 1)
        {
            fprintf(stderr, "Invalid sample ID. Using default\n");
            pxConfig->test = 0;
        }
    }
}



void WaitForUser()
{
    printf("\r\n--- press any key ---\r\n");
    AiOsGetChar();
}


void PrintSampleSetup(AiUInt8 boardHandle, AiUInt8 channelId)
{
    AiReturn               retVal = 0;
    char                   cOsVer[255];

    /* Api429ReadBSPVersionEx */
    struct api429_version_info x_VersionInfo;
    struct ty_ver_info versions[AI_MAX_VERSIONS];
    struct ty_version_out version_result = { 0 };
    struct api429_board_info board_info;
    struct api429_channel_info channel_info = { 0 };
    AiUInt32 i = 0;

    memset( &x_VersionInfo, 0, sizeof(x_VersionInfo) );


    printf("\r\nBoard: %s\r\n\n", Api429BoardNameGet(boardHandle) );

    AiOsGetOsVersion(cOsVer, sizeof(cOsVer));
    printf("Operating System: %s\n\n", cOsVer);


    retVal = Api429BoardInfoGet(boardHandle, &board_info);
    AIM_CHECK_RESULT(retVal);

    /* Print init Info */
    printf("Board information :\r\n");        
    printf("    %-20s:    %u\r\n",   "Board Serial", board_info.serial);
    printf("    %-20s:    %s\r\n",   "Server URL", board_info.server_url);
    printf("    %-20s:    %u\r\n",   "Number of Channels", board_info.num_channels);
    printf("    %-20s:    %08x\r\n", "Capability Flags", board_info.capability_flags);
    if( API429_BOARD_HAS_PXI_TRIGGER(&board_info) )
        printf("    --> %-20s\r\n",   "   Board supports PXI Trigger function ");
    if( API429_BOARD_TYPE_IS_EMBEDDED(&board_info) )
        printf("    --> %-20s\r\n",   "   Board is Embedded");
    if( API429_BOARD_TX_INHIBIT_IS_ACTIVATED(&board_info) )
        printf("    --> %-20s\r\n",   "   Board TX Inhibit is activated");


    retVal = Api429VersionGetAll(boardHandle, AI_MAX_VERSIONS, versions, &version_result);
    AIM_CHECK_RESULT(retVal);

    printf("\r\n");
    printf("Version info : \r\n");

    for(i = 0; i < version_result.ul_Count; i++)
    {
        printf("    %-20s:    %-64s\r\n", versions[i].ac_Description, versions[i].ac_FullVersion);
    }

    printf("\r\n");
    printf("\r\n");

    retVal = Api429ChannelInfoGet(boardHandle, channelId, &channel_info);
    AIM_CHECK_RESULT(retVal);

    printf("Properties of channel %d: \r\n", channelId);
    printf("    %-20s:    %s\r\n",   "Is able to transmit", API429_CHANNEL_CAN_TRANSMIT(channel_info) ? "Yes" : "No");
    printf("    %-20s:    %s\r\n",   "Is able to receive", API429_CHANNEL_CAN_RECEIVE(channel_info) ? "Yes" : "No");
    printf("    %-20s:    0x%x\r\n",   "Configuration flags", channel_info.config_flags);
    printf("    %-20s:    %s\r\n",   "Current Speed", channel_info.speed == API429_HI_SPEED ? "High" : "Low");

    printf("\r\n");
    printf("\r\n");

    return;
}


/* EOF */
