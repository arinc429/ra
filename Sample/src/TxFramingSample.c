/* SPDX-FileCopyrightText: 2001-2021 AIM GmbH <info@aim-online.com> */
/* SPDX-License-Identifier: MIT */

/******************************************************************************
 *  Includes
******************************************************************************/
#include "A429_sample.h"


/******************************************************************************
 *  Defines
******************************************************************************/


/******************************************************************************
 *  local Vars (Modul)
******************************************************************************/

/******************************************************************************
 *  extern Vars
******************************************************************************/


/************************************
      Some Makros
************************************/


/******************************************************************************
 *  global variables used only within this file
******************************************************************************/


/******************************************************************************
 *  local prototypes
******************************************************************************/
AiReturn SetupTx(AiUInt8 boardHandle, AiUInt8 channelId);

/******************************************************************************
 ******************************************************************************
 **
 ** Sample
 **
 ******************************************************************************
 ******************************************************************************/

AiReturn TxFramingSample( AiUInt8 boardHandle, AiUInt8 channelId)
{
    AiReturn retVal;
    char cKey;

    /* Api429TxStatusGet */
    AiUInt8 b_TxStatus;
    AiUInt32 l_GlbCnt;

    printf("Running TX Sample for channel %d \r\n", channelId);

    /* Setup a TX channel */
    retVal = SetupTx(boardHandle, channelId);
    AIM_CHECK_RESULT_RETURN(retVal);


    retVal = Api429ChannelStart(boardHandle, channelId);
    AIM_CHECK_RESULT_RETURN(retVal);
    printf("\n");

    printf("Press 'q' to quit or 'any key' to print the TX counter\r\n");
    cKey = 0;
    while (cKey != 'q'){
        retVal = Api429TxStatusGet(boardHandle, channelId, &b_TxStatus, &l_GlbCnt);
        AIM_CHECK_RESULT_RETURN(retVal);

        printf("\rApi429TxStatusGet:  %d Transfers sent", l_GlbCnt);

        cKey = AiOsGetChar();
    }
    printf("\r\n");

    /* Clean up */
    retVal = Api429ChannelHalt(boardHandle, channelId);
    AIM_CHECK_RESULT_RETURN(retVal);

    retVal = Api429TxStatusGet(boardHandle, channelId, &b_TxStatus, &l_GlbCnt);
    printf("\rApi429TxStatusGet:  %d Transfers total", l_GlbCnt);

    return 0;
} /* end: main */


static AiReturn SetupTransfer(AiUInt8 boardHandle, AiUInt8 channelId, AiUInt32 transferId, AiUInt32 data[4])
{
    AiReturn retVal;

    /* Api429CmdTxXferDef */
    struct api429_xfer x_Xfer;
    struct api429_xfer_out x_XferInfo;


    memset(&x_Xfer, 0, sizeof(x_Xfer));
    x_Xfer.xfer_id = transferId;
    x_Xfer.xfer_type = API429_TX_LAB_XFER;
    x_Xfer.err_type = API429_XFER_ERR_DIS;
    x_Xfer.buf_size = 4;
    x_Xfer.xfer_gap = 4;
    retVal = Api429TxXferCreate(boardHandle, channelId, &x_Xfer, &x_XferInfo);
    AIM_CHECK_RESULT_RETURN(retVal);

    if ((x_XferInfo.ul_Status != 0) || (x_XferInfo.ul_FreeMem == 0))
        printf("  Api429CmdTxXferDef was unable to allocate memory\r\n");

    /* Setting the data to be sent */
    retVal = Api429TxXferBufferWrite(boardHandle, channelId, x_Xfer.xfer_id, 1, x_Xfer.buf_size, data);
    AIM_CHECK_RESULT_RETURN(retVal);

    return retVal;
}

/* Create a simple framing, where transfer 1 is within a single
 * minor frame and the minor frame time is set to 10ms
 */
static AiReturn CreateSimpleFraming(AiUInt8 boardHandle, AiUInt8 channelId)
{
    AiReturn retVal;

    /* Api429TxMinorFrameCreate */
    AiUInt32  aul_Xfers[1];
    struct api429_mframe_in  x_MFrame;
    struct api429_mframe_out x_MFrameInfo;

    /* Api429TxMajorFrameCreate */
    AiUInt32 aul_Frames[1];


    /* change Minor Frame time to 10ms (default is 100ms).
     * Any other value is also possible */
    retVal = Api429TxFrameTimeSet(boardHandle, channelId, 10); /* set frame time to 10ms */
    AIM_CHECK_RESULT_RETURN(retVal);


    /* Send the transfer defined above within Minor Frame 0 */
    aul_Xfers[0] = 1;

    memset(&x_MFrame, 0, sizeof(x_MFrame));
    x_MFrame.ul_FrmId = 0;
    x_MFrame.ul_XferCnt = 1;
    x_MFrame.pul_Xfers = &aul_Xfers[0];
    retVal = Api429TxMinorFrameCreate(boardHandle, channelId, &x_MFrame, &x_MFrameInfo);
    AIM_CHECK_RESULT_RETURN(retVal);

    /* Send Minor Frame 0 within the major frame. */
    aul_Frames[0] = 0;
    retVal = Api429TxMajorFrameCreate(boardHandle, channelId, 1, aul_Frames);
    AIM_CHECK_RESULT_RETURN(retVal);

    return retVal;
}

AiReturn SetupTx(AiUInt8 boardHandle, AiUInt8 channelId)
{
    AiReturn retVal;
    AiUInt32 ul_Index;

    AiUInt32 aul_XferLWord[4];


    printf("\r\nPreparing TX channel\r\n");

    retVal = Api429TxInit(boardHandle, channelId, API429_TX_MODE_FRAMING, 0);
    AIM_CHECK_RESULT_RETURN(retVal);

    /*retVal = Api429TxAmplitudeSet( boardHandle, channelId, 11.0, NULL);  // calling this function is not mandatory, use default instead */

    /*retVal = Api429ChannelSpeedSet( boardHandle, channelId, API429_HI_SPEED );  // calling this function is not mandatory, use default instead */


    /* Setting the data to be sent */
    for (ul_Index = 0; ul_Index < 4; ul_Index++)
        aul_XferLWord[ul_Index] = 0x0000F000 | (ul_Index + 1);

    /* setup the transfer with this data */
    retVal = SetupTransfer(boardHandle, channelId, 1, aul_XferLWord);
    AIM_CHECK_RESULT_RETURN(retVal);



    /* create a simple framing */
    retVal = CreateSimpleFraming(boardHandle, channelId);
    AIM_CHECK_RESULT_RETURN(retVal);

    return 0;
}

/* EOF */
