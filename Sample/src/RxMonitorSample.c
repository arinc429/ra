/* SPDX-FileCopyrightText: 2001-2021 AIM GmbH <info@aim-online.com> */
/* SPDX-License-Identifier: MIT */

/******************************************************************************
 *  Includes
******************************************************************************/
#include "A429_sample.h"

/******************************************************************************
 *  Defines
******************************************************************************/




/************************************
      Some Makros
************************************/



/******************************************************************************
 *  local prototypes
******************************************************************************/
AiReturn SetupRm(AiUInt8 boardHandle, AiUInt8 channelId);
AiReturn ShowRm(AiUInt8 boardHandle, AiUInt8 channelId);


/******************************************************************************
 ******************************************************************************
 **
 ** Sample
 **
 ******************************************************************************
 ******************************************************************************/

AiReturn RxMonitorSample( AiUInt8 boardHandle, AiUInt8 channelId)
{
    AiReturn retVal = 0;
    char cKey;
    struct api429_rm_setup monitor_setup = { 0 };

    /* Api429RxStatusGet */
    AiUInt8 b_RxStatus;
    AiUInt32 l_MsgCnt, l_ErrCnt;

    /* Api429RmStatusGet */
    AiUInt8 b_RmStatus;
    AiUInt16 w_Msw;

    printf("Running RX Monitor Sample \r\n");


    /* Setup receive monitor Channel */
    retVal = SetupRm(boardHandle, channelId);
    AIM_CHECK_RESULT_RETURN(retVal);

    retVal = Api429RmInfoGet(boardHandle, channelId, &monitor_setup);
    AIM_CHECK_RESULT_RETURN(retVal);

    printf("Set-up monitor with following settings: \r\n");
    printf("    %-20s:    %d\r\n", "Monitor type", monitor_setup.mode);
    printf("    %-20s:    %d\r\n", "Buffer size", monitor_setup.size_in_entries);
    printf("    %-20s:    %d\r\n", "Trace after trigger", monitor_setup.tat_count);
    printf("    %-20s:    %d\r\n", "Interrupt mode", monitor_setup.interrupt_mode);


    /* start reception now */
    retVal = Api429ChannelStart(boardHandle, channelId);
    AIM_CHECK_RESULT_RETURN(retVal);

    /* give Api429CmdRxStart some time to complete */
    AiOsSleep(100);

    /* no explicit call to Api429CmdRmStart is needed */

    printf("\n");
    printf("Press 'q' to quit and show the receive buffer or 'any key' to print the receive counter\r\n");

    do
    {
        retVal = Api429RxStatusGet(boardHandle, channelId, &b_RxStatus, &l_MsgCnt, &l_ErrCnt);
        AIM_CHECK_RESULT_RETURN(retVal);

        retVal = Api429RmStatusGet(boardHandle, channelId, &b_RmStatus, &w_Msw);
        AIM_CHECK_RESULT_RETURN(retVal);

        printf("\r%d Transfers  RM-Status:%X  MSW:%08X ", l_MsgCnt, b_RmStatus, w_Msw);

        cKey = AiOsGetChar();
    } while (cKey != 'q');

    printf("\r\n\n");

    ShowRm(boardHandle, channelId);

    /* Halt the RM and RX channel operation */
    retVal = Api429ChannelHalt(boardHandle, channelId);
    AIM_CHECK_RESULT_RETURN(retVal);

    return 0;
} /* end: main */


AiReturn SetupRm(AiUInt8 boardHandle, AiUInt8 channelId)
{
    AiReturn  retVal = 0;
    AiUInt32  ul_LabIndex;
    struct api429_rm_setup setup = { 0 };
    struct api429_rm_label_config label_config = { 0 };


    printf("Setting up RM for channel %d\r\n", channelId);

    retVal = Api429RxInit(boardHandle, channelId, AiFalse, AiFalse);
    AIM_CHECK_RESULT_RETURN(retVal);


    setup.mode = API429_RM_MODE_LOC;
    setup.size_in_entries = API429_RM_MON_DEFAULT_SIZE;
    setup.tat_count = API429_RM_CONTINUOUS_CAPTURE;
    setup.interrupt_mode = API429_RM_IR_DIS;

    retVal = Api429RmCreate(boardHandle, channelId, &setup);
    AIM_CHECK_RESULT_RETURN(retVal);


    /*Note: to enable SDI sorting, use Api429RxInit with sdi_sorting_enabled set to AiTrue. */

    /* enable all labels...
     * loop over all labels 0..255 = 256 possible labels */
    for (ul_LabIndex = 0; ul_LabIndex <= 255; ul_LabIndex++)
    {

        label_config.label_id = ul_LabIndex;
        label_config.sdi = 0;
        label_config.enable = AiTrue;
        retVal = Api429RmLabelConfigure(boardHandle, channelId, &label_config);
        AIM_CHECK_RESULT_RETURN(retVal);
    }

    return retVal;
}


AiReturn ShowRm(AiUInt8 boardHandle, AiUInt8 channelId)
{
    AiUInt32 i;
    AiReturn retVal;

    /* Api429RmDataRead */
    struct api429_rcv_stack_entry ax_SData[32];
    AiUInt16 uw_Count;


    printf("Reading some RM data\r\n");

    /* Read up to 32 entries from the ring buffer. If more data has to be read, either
     * increase the value for parameter ux_EntriesToRead or repeatedly call Api429RmDataRead*/
    retVal = Api429RmDataRead(boardHandle, channelId, 32, &uw_Count, ax_SData);
    AIM_CHECK_RESULT_RETURN(retVal);

    /* Please see the reference manual for details on this data.
     * The BRW (buffer report word) contains information about
     * gap time, errors detected and originating channel */
    printf("   lData       tm_tag        gap  channel  error\r\n");
    printf("  -------------------------------------------------\r\n");
    for (i = 0; i < uw_Count; i++) {
        printf("  %08X  %02d:%02d:%02d.%06d  %d     %d       %x\r\n", ax_SData[i].ldata,
            ax_SData[i].brw.b.hours, ax_SData[i].tm_tag.b.minutes, ax_SData[i].tm_tag.b.seconds, ax_SData[i].tm_tag.b.microseconds,
            ax_SData[i].brw.b.gap, ax_SData[i].brw.b.channel + 1, ax_SData[i].brw.b.e_type);
    }

    return retVal;
}

/* EOF */
