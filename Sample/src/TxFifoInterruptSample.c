/* SPDX-FileCopyrightText: 2009-2021 AIM GmbH <info@aim-online.com> */
/* SPDX-License-Identifier: MIT */

#include "A429_sample.h"

#define NUMBER_OF_LABELS           4
#define NUMBER_OF_FIFO_ENTRIES     NUMBER_OF_LABELS + 2

/******************************************************************************
*  local Vars (Modul)
*  For easier use within interrupt context
******************************************************************************/
static int interruptCount = 0;

/* struct to hold all information from an interrupt
so that we can print it at the end of the sample */
struct sample_basic_int_info{
    AiUInt8 boardHandle;
    enum api429_event_type type;
    AiUInt8 channelId;
    AiUInt8 tag;
    AiUInt32 ul_Lla;
    AiUInt32 ul_Llb;
    AiUInt32 ul_Llc;
    AiUInt32 ul_Lld;
};

static struct sample_basic_int_info x_Int_Info[20];

/******************************************************************************
*  local prototypes
******************************************************************************/
static void AI_CALL_CONV channelEventCallback(AiUInt8 boardHandle, AiUInt8 channelId, enum api429_event_type type, struct api429_intr_loglist_entry* x_Info);




AiUInt32 getTagFromLla(AiUInt32 lla)
{
    return (lla>>20) & 0xFF;
}

/* Send four Arinc429 data word write entries and two interrupt event 
 * entries to the TX FIFO 
 */
static AiReturn WriteEntriesToFifo(AiUInt8 boardHandle, AiUInt8 channelId)
{
    AiReturn retVal;
    AiUInt32 ulEntriesWritten;
    struct api429_tx_fifo_entry fifo_input[NUMBER_OF_FIFO_ENTRIES];

    retVal = Api429TxFifoDataWordCreate(&fifo_input[0], 0xF001, 4, 0);
    AIM_CHECK_RESULT_RETURN(retVal);

    retVal = Api429TxFifoDataWordCreate(&fifo_input[1], 0xF002, 4, 0);
    AIM_CHECK_RESULT_RETURN(retVal);

    retVal = Api429TxFifoInterruptCreate(&fifo_input[2], 0);
    AIM_CHECK_RESULT_RETURN(retVal);

    retVal = Api429TxFifoDataWordCreate(&fifo_input[3], 0xF003, 4, 0);
    AIM_CHECK_RESULT_RETURN(retVal);

    retVal = Api429TxFifoDataWordCreate(&fifo_input[4], 0xF004, 4, 0);
    AIM_CHECK_RESULT_RETURN(retVal);

    retVal = Api429TxFifoInterruptCreate(&fifo_input[5], 0xFF);
    AIM_CHECK_RESULT_RETURN(retVal);

    retVal = Api429TxFifoWrite(boardHandle, channelId, NUMBER_OF_FIFO_ENTRIES, fifo_input, AiTrue, &ulEntriesWritten);
    AIM_CHECK_RESULT_RETURN(retVal);

    return retVal;
}



/* ######################################################################################*/
/*                                                                                       */
/*                                     Sample entry point                                */
/*                                                                                       */
/* ######################################################################################*/
AiReturn TxFifoInterruptSample(AiUInt8 boardHandle, AiUInt8 channelId)
{
    AiReturn retVal;
    char cKey;
    int i;
    AiUInt32 expectedTxCount = 0;

    /* Api429TxStatusGet */
    AiUInt8 b_TxStatus;
    AiUInt32 l_GlbCnt;


    printf("Running TX FIFO Sample for channel %d \r\n", channelId);


    /* Init the interrupt counter*/
    interruptCount = 0;

    printf("Now install the Event Handler Functions \r\n");
    retVal = Api429ChannelCallbackRegister(boardHandle, channelId, 0, channelEventCallback);
    AIM_CHECK_RESULT_RETURN(retVal);


    /**************************************/
    /* Prepare channel for fifo           */
    /**************************************/

    /* Enable TX FIFO mode */
    retVal = Api429TxInit(boardHandle, channelId, API429_TX_MODE_FIFO, 0);
    AIM_CHECK_RESULT_RETURN(retVal);


    /* Start the TX channel */
    retVal = Api429ChannelStart(boardHandle, channelId);
    AIM_CHECK_RESULT_RETURN(retVal);
    printf("\n");



    /***************************************/
    /* Show status and fill FIFO on demand */
    /***************************************/

    printf("Press 'q' to quit, 's' to send or any other key to print the TX counter\r\n");
    cKey = 0;
    while (cKey != 'q'){

        if (cKey == 's')
        {
            /* put new data into the FIFO */
            retVal = WriteEntriesToFifo(boardHandle, channelId);
            AIM_CHECK_RESULT_RETURN(retVal);

            expectedTxCount += NUMBER_OF_LABELS;
        }


        retVal = Api429TxStatusGet(boardHandle, channelId, &b_TxStatus, &l_GlbCnt);
        AIM_CHECK_RESULT_RETURN(retVal);

        printf("\rApi429TxStatusGet:  %d of %d Transfers sent", l_GlbCnt, expectedTxCount);

        cKey = AiOsGetChar();
    }
    printf("\r\n");



    printf("\nShowing received channel events:\n");

    printf("Brd  Type Chn Tag\n");
    for (i = 0; i < interruptCount; i++)
    {
        printf("%3d  %4d %3d  %02X\n",
            x_Int_Info[i].boardHandle,
            x_Int_Info[i].type,
            x_Int_Info[i].channelId,
            getTagFromLla(x_Int_Info[i].ul_Lla));
    }

    /* clean up */

    retVal = Api429ChannelHalt(boardHandle, channelId);
    AIM_CHECK_RESULT_RETURN(retVal);

    printf("\r\nNow uninstall the Event Handler Functions \r\n");
    retVal = Api429ChannelCallbackUnregister(boardHandle, channelId, 0);
    AIM_CHECK_RESULT_RETURN(retVal);

    return retVal;
}


#define API429_CHANNEL_EVENT_TAG(channel_event) \
    (((channel_event)->ul_Lla >> 20) & 0xFF)


/******************************************************************************
**
** Sample: Tx event handler function
**         Please refer to function Api429ChannelCallbackRegister() in the reference
**         manual for details of the user interrupt function
**
******************************************************************************/
static void AI_CALL_CONV channelEventCallback(AiUInt8 boardHandle, AiUInt8 channelId, enum api429_event_type type, struct api429_intr_loglist_entry* x_Info)
{
    /* we expect the type to be API429_EVENT_TX_FIFO */
    

    /* Sample: copy interrupt information so that we can print it out later */
    if (interruptCount<20){
        x_Int_Info[interruptCount].boardHandle = boardHandle;
        x_Int_Info[interruptCount].type   = type;
        x_Int_Info[interruptCount].channelId = channelId;
        x_Int_Info[interruptCount].tag    = API429_CHANNEL_EVENT_TAG(x_Info);
        x_Int_Info[interruptCount].ul_Lla = x_Info->ul_Lla;
        x_Int_Info[interruptCount].ul_Llb = x_Info->ul_Llb;
        x_Int_Info[interruptCount].ul_Llc = x_Info->x_Llc.ul_All;
        x_Int_Info[interruptCount].ul_Lld = x_Info->ul_Lld;
    }
    interruptCount++;
}
