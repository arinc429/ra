/* SPDX-FileCopyrightText: 2001-2021 AIM GmbH <info@aim-online.com> */
/* SPDX-License-Identifier: MIT */

/******************************************************************************
 *  Includes
******************************************************************************/
#include "A429_sample.h"


/******************************************************************************
 *  Defines
******************************************************************************/


/******************************************************************************
 *  local Vars (Modul)
******************************************************************************/

/******************************************************************************
 *  extern Vars
******************************************************************************/


/************************************
      Some Makros
************************************/


/******************************************************************************
 *  global variables used only within this file
******************************************************************************/


/******************************************************************************
 *  local prototypes
******************************************************************************/
static AiReturn SetupTxWithDyntag(AiUInt8 boardHandle, AiUInt8 channelId );

/******************************************************************************
 ******************************************************************************
 **
 ** Sample
 **
 ******************************************************************************
 ******************************************************************************/

AiReturn TxDyntagSample(AiUInt8 boardHandle, AiUInt8 channelId)
{
    AiReturn retVal;
    char cKey;

    /* Api429TxStatusGet */
    AiUInt8 b_TxStatus;
    AiUInt32 l_GlbCnt;

    printf("Running TX Dyntag Sample for channel %d \r\n", channelId);

    /* Setup a TX channel */
    SetupTxWithDyntag(boardHandle, channelId);

    /* Start the channel */
    retVal = Api429ChannelStart(boardHandle, channelId);
    AIM_CHECK_RESULT_RETURN(retVal);
    printf("\n");

    printf("Press 'q' to quit or 'any key' to print the TX counter\r\n");
    cKey = 0;
    while (cKey != 'q'){
        retVal = Api429TxStatusGet(boardHandle, channelId, &b_TxStatus, &l_GlbCnt);
        AIM_CHECK_RESULT_RETURN(retVal);

        printf("\rApi429TxStatusGet:  %d Transfers sent", l_GlbCnt);

        cKey = AiOsGetChar();
    }
    printf("\r\n");

    /* Clean up */
    retVal = Api429ChannelHalt(boardHandle, channelId);
    AIM_CHECK_RESULT_RETURN(retVal);

    retVal = Api429TxStatusGet(boardHandle, channelId, &b_TxStatus, &l_GlbCnt);
    printf("\rApi429TxStatusGet:  %d Transfers total", l_GlbCnt);

    return 0;
} /* end: main */



static AiReturn SetupTxWithDyntag(AiUInt8 boardHandle, AiUInt8 channelId)
{
    AiReturn retVal;

    /* Api429CmdTxXferDef */
    struct api429_xfer x_Xfer;
    struct api429_xfer_out x_XferInfo;

    /* Api429TxMajorFrameCreate */
    AiUInt32  aul_Frames[1];

    /* Api429TxMinorFrameCreate */
    AiUInt32  aul_Xfers[1];
    struct api429_mframe_in  x_MFrame;
    struct api429_mframe_out x_MFrameInfo;

    /* Api429TxXferDyntagAssign */
    struct api429_lxfer_dyntag x_Dyntag;


    printf("\r\nPreparing TX channel\r\n");

    printf("  Setting channel to support Dyntags\r\n");
    printf("  Please note: no multibuffering is possible on channels configured for Dyntag \n");
    retVal = Api429TxInit(boardHandle, channelId, API429_TX_MODE_FRAMING_DYNTAG, 0);
    AIM_CHECK_RESULT_RETURN(retVal);


    /* Setting up a transfer */
    memset(&x_Xfer, 0, sizeof(x_Xfer));
    x_Xfer.xfer_id = 1;
    x_Xfer.xfer_type = API429_TX_LAB_XFER;
    x_Xfer.buf_size = 1;
    x_Xfer.xfer_gap = 4;
    retVal = Api429TxXferCreate(boardHandle, channelId, &x_Xfer, &x_XferInfo);
    AIM_CHECK_RESULT_RETURN(retVal);

    if ((x_XferInfo.ul_Status != 0) || (x_XferInfo.ul_FreeMem == 0))
        printf("  Api429CmdTxXferDef was unable to allocate memory\r\n");



    /* Setting the values for the Dyntag (on label 1)
     * This will send data as follows:
     *             0x00008801, 0x00008901, 0x00008A01, 0x00008B01, ...
     * Once 0x0000FF00 is reached the data will be reset to 0x00001100
     */
    printf("  Setting the values for the Dyntag\n");
    x_Dyntag.ul_Mode       = 1;
    x_Dyntag.ul_UpperLimit = 0x0000FF00;
    x_Dyntag.ul_LowerLimit = 0x00001100;
    x_Dyntag.ul_StepSize   = 0x00000100;
    x_Dyntag.ul_Mask       = 0x0000FF00;
    x_Dyntag.ul_StartValue = 0x00008800/* Start Data */
                             + 0x01/* Label ID (0..0xFF) */;

    retVal = Api429TxXferDyntagAssign(boardHandle, channelId, x_Xfer.xfer_id, &x_Dyntag);
    AIM_CHECK_RESULT_RETURN(retVal);



    /* Send all the transfers defined above within Minor Frame 0 */
    aul_Xfers[0] = 1;

    memset(&x_MFrame, 0, sizeof(x_MFrame));
    x_MFrame.ul_FrmId = 0;
    x_MFrame.ul_XferCnt = 1;
    x_MFrame.pul_Xfers = &aul_Xfers[0];
    retVal = Api429TxMinorFrameCreate(boardHandle, channelId, &x_MFrame, &x_MFrameInfo);
    AIM_CHECK_RESULT_RETURN(retVal);

    /* Send Minor Frame 0 within the major frame. */
    aul_Frames[0] = 0;
    retVal = Api429TxMajorFrameCreate(boardHandle, channelId, 1, aul_Frames);
    AIM_CHECK_RESULT_RETURN(retVal);

    return 0;
}

/* EOF */
