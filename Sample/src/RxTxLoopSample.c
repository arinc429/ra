/* SPDX-FileCopyrightText: 2019-2021 AIM GmbH <info@aim-online.com> */
/* SPDX-License-Identifier: MIT */

#include "A429_sample.h"



AiReturn SetupChannelLoop(AiUInt8 uc_Module, AiUInt8 uc_RxChn, AiUInt8 uc_TxChn, AiUInt32 uc_AddSubVal);




/* ######################################################################################*/
/*                                                                                       */
/*                                     Sample entry point                                */
/*                                                                                       */
/* ######################################################################################*/

AiReturn RxTxLoopSample(AiUInt8 boardHandle, AiUInt8 channelId)
{
    AiReturn retVal = 0;

    /* Api429BoardInfoGet*/
    struct api429_board_info board_info;
    AiUInt32 emb_flag = 0;

    char cKey;
    char szDummy[255];

    AiUInt8  rxStatus;
    AiUInt32 rxCnt;
    AiUInt32 rxErrCnt;

    AiUInt32 TxChannel;
    AiUInt32 valueOffset;

    printf("Running RxTxLoopSample for channel %d \r\n", channelId);

    retVal = Api429BoardInfoGet(boardHandle, &board_info);
    AIM_CHECK_RESULT_RETURN(retVal);
    emb_flag = API429_BOARD_TYPE_IS_EMBEDDED(&board_info);
    if (emb_flag == AiTrue)
    {
        printf("\r\n*** Sample terminated:  Rx-Tx Loop Function is not supported on Embedded Board types !\r\n");
        return 0;
    }

    printf("To which Tx channel shall the data be forwarded ? ");
    AiOsGetString(szDummy, sizeof(szDummy), "1");
    sscanf(szDummy, "%d", &TxChannel);

    printf("Which offset shall be added to the data (default: 0) ? ");
    AiOsGetString(szDummy, sizeof(szDummy), "0");
    sscanf(szDummy, "%d", &valueOffset);

    retVal = SetupChannelLoop(boardHandle, channelId, (AiUInt8)TxChannel, valueOffset);
    AIM_CHECK_RESULT_RETURN(retVal);

    printf("All data coming in from channel %d will be forwarded to channel %d\n\n", channelId, TxChannel);

    printf("Press 'q' to quit, any other key to print the status\r\n");
    cKey = 0;
    while (cKey != 'q')
    {
        retVal = Api429RxStatusGet(boardHandle, channelId, &rxStatus, &rxCnt, &rxErrCnt);
        AIM_CHECK_RESULT_RETURN(retVal);

        printf("\r %d+%d Transfers received", rxCnt, rxErrCnt);

        cKey = AiOsGetChar();
    }
    printf("\r\n");

    Api429ChannelHalt(boardHandle, channelId);
    Api429ChannelHalt(boardHandle, (AiUInt8)TxChannel);

    return 0;
}


///////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////
//
// Loop/Pollution Samples
//
///////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////
AiReturn SetupChannelLoop(AiUInt8 uc_Module, AiUInt8 uc_RxChn, AiUInt8 uc_TxChn, AiUInt32 valueOffset)
{
    AiReturn retVal;
    AiUInt32 ul_SdiIndex;
    AiUInt32 ul_LabIndex;
    AiUInt8  uc_Status;
    AiUInt32 ul_FreeMem;
    struct api429_rcv_pb_cmd x_PollBlk;
    struct api429_rx_label_setup label_setup;

    retVal = Api429TxInit(uc_Module, uc_TxChn, API429_TX_MODE_LOOP, 0);
    AIM_CHECK_RESULT_RETURN(retVal);

    retVal = Api429ChannelStart(uc_Module, uc_TxChn);
    AIM_CHECK_RESULT_RETURN(retVal);

    retVal = Api429RxInit(uc_Module, uc_RxChn, 0, 0);
    AIM_CHECK_RESULT_RETURN(retVal);

    retVal = Api429RxDataLoopAssign(uc_Module, uc_RxChn, uc_TxChn);
    AIM_CHECK_RESULT_RETURN(retVal);

    for (ul_SdiIndex = 0; ul_SdiIndex < 1; ul_SdiIndex++)
    {
        for (ul_LabIndex = 0; ul_LabIndex <= 254; ul_LabIndex++)
        {
            label_setup.label = ul_LabIndex;
            label_setup.sdi = ul_SdiIndex;
            label_setup.con = 1;
            label_setup.irCon = 0;
            label_setup.irIndex = 0;
            label_setup.bufSize = 10;

            retVal = Api429RxLabelConfigure(uc_Module, uc_RxChn, &label_setup, &uc_Status, &ul_FreeMem);
            AIM_CHECK_RESULT_RETURN(retVal);
        }
    }

    for (ul_SdiIndex = 0; ul_SdiIndex < 1; ul_SdiIndex++)
    {
        for (ul_LabIndex = 0; ul_LabIndex < 10; ul_LabIndex++)
        {
            /* Note: the Loop/Pollution has only limited possibilities to manipulate data */
            x_PollBlk.pb_id = (AiUInt8)(ul_LabIndex + 1);
            x_PollBlk.asc = 0;	// addition
            x_PollBlk.and_mask = 0xffffffff;
            x_PollBlk.or_mask = 0;
            x_PollBlk.xor_mask = 0;
            x_PollBlk.addsub_val = valueOffset;
            retVal = Api429RxPollutionConfigure(uc_Module,
                uc_RxChn,
                (AiUInt8)ul_LabIndex + 1,
                (AiUInt8)ul_SdiIndex,
                API429_ENA,
                &x_PollBlk);
            AIM_CHECK_RESULT_RETURN(retVal);
        }
    }

    retVal = Api429ChannelStart(uc_Module, uc_RxChn);
    AIM_CHECK_RESULT_RETURN(retVal);

    return 0;
}

/* EOF */

