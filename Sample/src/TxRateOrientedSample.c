/* SPDX-FileCopyrightText: 2001-2021 AIM GmbH <info@aim-online.com> */
/* SPDX-License-Identifier: MIT */

/******************************************************************************
 *  Includes
******************************************************************************/
#include "A429_sample.h"


/******************************************************************************
 *  Defines
******************************************************************************/

#define FRM_1_ID    1
#define FRM_1_RATE  15

#define FRM_2_ID    2
#define FRM_2_RATE  20

#define FRM_3_ID    3
#define FRM_3_RATE  61


/******************************************************************************
 *  extern Vars
******************************************************************************/


/************************************
      Some Makros
************************************/


/******************************************************************************
 *  global variables used only within this file
******************************************************************************/


/******************************************************************************
 *  local prototypes
******************************************************************************/
AiReturn SetupTxRateOriented(AiUInt8 boardHandle, AiUInt8 channelId );


/******************************************************************************
 ******************************************************************************
 **
 ** Sample
 **
 ******************************************************************************
 ******************************************************************************/

AiReturn TxRateOrientedSample(AiUInt8 boardHandle, AiUInt8 channelId)
{
    AiReturn retVal;
    char cKey;

    /* Api429TxStatusGet */
    AiUInt8 b_TxStatus;
    AiUInt32 l_GlbCnt;

    printf("Running TX Rate Oriented Sample for channel %d \r\n", channelId);


    /* Setup a TX channel with a rate oriented framing */
    SetupTxRateOriented(boardHandle, channelId);

    /* Start the channel */
    retVal = Api429ChannelStart(boardHandle, channelId);
    AIM_CHECK_RESULT_RETURN(retVal);
    printf("\n");

    printf("Press 'q' to quit or 'any key' to print the TX counter\r\n");
    cKey = 0;
    while (cKey != 'q'){
        retVal = Api429TxStatusGet(boardHandle, channelId, &b_TxStatus, &l_GlbCnt);
        AIM_CHECK_RESULT_RETURN(retVal);

        printf("\rApi429TxStatusGet:  %d Transfers sent", l_GlbCnt);

        cKey = AiOsGetChar();
    }
    printf("\r\n");

    /* Clean up */
    retVal = Api429ChannelHalt(boardHandle, channelId);
    AIM_CHECK_RESULT_RETURN(retVal);

    retVal = Api429TxStatusGet(boardHandle, channelId, &b_TxStatus, &l_GlbCnt);
    printf("\rApi429TxStatusGet:  %d Transfers sent", l_GlbCnt);

    return 0;
} /* end: main */


static AiReturn SetupSingleTransferWithRate(AiUInt8 boardHandle, AiUInt8 channelId, AiUInt32 xferId, AiUInt32 dataWord, AiUInt32 rate)
{
    AiReturn retVal;

    /* Api429CmdTxXferDef */
    struct api429_xfer x_Xfer;
    struct api429_xfer_out x_XferInfo;

    /* Api429TxXferBufferWrite */
    AiUInt32  aul_XferLWord[1];

    /* Setting up a transfer with FRM_1_ID and FRM_1_RATE */
    memset(&x_Xfer, 0, sizeof(x_Xfer));
    x_Xfer.xfer_id = xferId;
    x_Xfer.xfer_type = API429_TX_LAB_XFER;
    x_Xfer.buf_size = 1;
    x_Xfer.xfer_gap = 4;
    retVal = Api429TxXferCreate(boardHandle, channelId, &x_Xfer, &x_XferInfo);
    AIM_CHECK_RESULT_RETURN(retVal);

    if ((x_XferInfo.ul_Status != 0) || (x_XferInfo.ul_FreeMem == 0))
        printf("  Api429CmdTxXferDef was unable to allocate memory\r\n");

    /* Setting the data to be sent */
    aul_XferLWord[0] = dataWord;
    retVal = Api429TxXferBufferWrite(boardHandle, channelId, xferId, 1, x_Xfer.buf_size, aul_XferLWord);
    AIM_CHECK_RESULT_RETURN(retVal);

    /* Set the rate */
    retVal = Api429TxXferRateAdd(boardHandle, channelId, xferId, rate);
    AIM_CHECK_RESULT_RETURN(retVal);

    return retVal;
}


/* Set up the framing of a TX channel, so that three transfers are repeatedly sent.
 * The three transfers have a transmission rate of 15, 20 and 61 ms
 */
AiReturn SetupTxRateOriented(AiUInt8 boardHandle, AiUInt8 channelId)
{
    AiReturn retVal;
    AiUInt32 ul_Tmp;



    printf("\r\nPreparing TX channel with automated framing\r\n");

    retVal = Api429TxInit(boardHandle, channelId, API429_TX_MODE_RATE_CONTROLLED, 0);
    AIM_CHECK_RESULT_RETURN(retVal);

    /*retVal = Api429TxAmplitudeSet( boardHandle, channelId, 11.0, NULL);  // calling this function is not mandatory, we use default instead */

    /*retVal = Api429ChannelSpeedSet( boardHandle, channelId, API429_HI_SPEED );  // calling this function is not mandatory, we use default instead */




    retVal = SetupSingleTransferWithRate(boardHandle, channelId, FRM_1_ID, 0x0000F001, FRM_1_RATE);
    AIM_CHECK_RESULT_RETURN(retVal);


    retVal = SetupSingleTransferWithRate(boardHandle, channelId, FRM_2_ID, 0x0000F002, FRM_2_RATE);
    AIM_CHECK_RESULT_RETURN(retVal);


    retVal = SetupSingleTransferWithRate(boardHandle, channelId, FRM_3_ID, 0x0000F003, FRM_3_RATE);
    AIM_CHECK_RESULT_RETURN(retVal);





    /* optional: create framing here, so that the transmission can later start without delays */
    retVal = Api429TxPrepareFraming(boardHandle, channelId);
    AIM_CHECK_RESULT_RETURN(retVal);




    /* optional: check if any rate was rounded - has to be done after the framing was created */
    retVal = Api429TxXferRateShow(boardHandle, channelId, FRM_1_ID, &ul_Tmp);
    AIM_CHECK_RESULT_RETURN(retVal);
    if (ul_Tmp == FRM_1_RATE)
        printf("    Xfer 1:  the rate of %dms was set\n", FRM_1_RATE);
    else
        printf("    Xfer 1:  the rate of %dms was rounded to %dms\n", FRM_1_RATE, ul_Tmp);

    retVal = Api429TxXferRateShow(boardHandle, channelId, FRM_2_ID, &ul_Tmp);
    AIM_CHECK_RESULT_RETURN(retVal);
    if (ul_Tmp == FRM_2_RATE)
        printf("    Xfer 2:  the rate of %dms was set\n", FRM_2_RATE);
    else
        printf("    Xfer 2:  the rate of %dms was rounded to %dms\n", FRM_2_RATE, ul_Tmp);

    retVal = Api429TxXferRateShow(boardHandle, channelId, FRM_3_ID, &ul_Tmp);
    AIM_CHECK_RESULT_RETURN(retVal);
    if (ul_Tmp == FRM_3_RATE)
        printf("    Xfer 3:  the rate of %dms was set\n", FRM_3_RATE);
    else
        printf("    Xfer 3:  the rate of %dms was rounded to %dms\n", FRM_3_RATE, ul_Tmp);


    return 0;
}

/* EOF */
