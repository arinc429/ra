/* SPDX-FileCopyrightText: 2001-2021 AIM GmbH <info@aim-online.com> */
/* SPDX-License-Identifier: MIT */

/******************************************************************************

    Description:

    Setup an A429 Receiver

******************************************************************************/


/******************************************************************************
 *  Includes
******************************************************************************/
#include "A429_sample.h"

/******************************************************************************
 *  Defines
******************************************************************************/


/************************************
      Some Makros
************************************/


/******************************************************************************
 *  local prototypes
******************************************************************************/
AiReturn SetupRx(AiUInt8 boardHandle, AiUInt8 channelId);
AiReturn ReadRxData(AiUInt8 boardHandle, AiUInt8 channelId);


/******************************************************************************
 ******************************************************************************
 **
 ** Sample
 **
 ******************************************************************************
 ******************************************************************************/

AiReturn RxSample(AiUInt8 boardHandle, AiUInt8 channelId)
{
    AiReturn retVal = 0;
    char cKey;

    /* Api429RxStatusGet */
    AiUInt8 b_RxStatus;
    AiUInt32 l_MsgCnt, l_ErrCnt;

    printf("Running RX Sample \r\n");

    /* Setup Rx Channel */
    retVal = SetupRx(boardHandle, channelId);
    AIM_CHECK_RESULT_RETURN(retVal);


    /* start reception now */
    retVal = Api429ChannelStart(boardHandle, channelId);
    AIM_CHECK_RESULT_RETURN(retVal);

    /* give Api429CmdRxStart some time to complete */
    AiOsSleep(100);
    printf("Press 'q' to quit and show the receive buffer or 'any key' to print the receive counter\r\n");

    printf("\n");
    do
    {
        retVal = Api429RxStatusGet(boardHandle, channelId, &b_RxStatus, &l_MsgCnt, &l_ErrCnt);
        AIM_CHECK_RESULT_RETURN(retVal);

        printf("\rApi429CmdRxStatusRead:  %d Transfers ", l_MsgCnt);

        cKey = AiOsGetChar();
    } while (cKey != 'q');

    printf("\r\n");

    /* Halt the channel operation */
    retVal = Api429ChannelHalt(boardHandle, channelId);
    AIM_CHECK_RESULT_RETURN(retVal);

    /* In this sample ReadRxData is called after Api429CmdRxHalt, to show how
     * the same data looks like, when different ways to obtain the data where used.
     * Of course the RX data can also be read when the RX channel is active */
    ReadRxData(boardHandle, channelId);

    return 0;
} /* end: main */


/* for each of the label, enable it for reception
 * and define a receive buffer size of 6, which means
 * that for each label, the latest 6 received label
 * words are stored (--> label history) */
static AiReturn SetupLabel(AiUInt8 boardHandle, AiUInt8 channelId, AiUInt32 labelId)
{
    AiReturn  retVal = 0;
    AiUInt32  i;

    /* Api429RxLabelConfigure */
    AiUInt8   uc_Status;
    AiUInt32  ul_FreeMem;

    /* Api429RxLabelBufferWrite */
    struct api429_rx_buf_entry  ax_LData[6];
    struct api429_rx_label_setup label_setup;


    label_setup.label = labelId;
    label_setup.sdi = 0;
    label_setup.con = API429_ENA;
    label_setup.irCon = 0;
    label_setup.irIndex = 0;
    label_setup.bufSize = 6;
    retVal = Api429RxLabelConfigure(boardHandle, channelId, &label_setup, &uc_Status, &ul_FreeMem);
    AIM_CHECK_RESULT_RETURN(retVal);

    if (0 != uc_Status)
    {
        printf("  Api429RxLabelConfigure was unable to allocate a label buffer\r\n");
        printf("  Not enough memory on board. Status: %i   free memory: 0x%x\r\n", uc_Status, ul_FreeMem);
        return 1;
    }

    /* the given array is used for initialization of
    the label receive buffers */
    for (i = 0; i < 6; i++)
        ax_LData[i].lab_data = 0x12345678L;

    /* calling this function is not mandatory, but can be used to initialize to initialize
    * the RX buffer e.g. with some easy recognizable values or with zero */
    retVal = Api429RxLabelBufferWrite(boardHandle, channelId,
        (AiUInt8)labelId,
        0,               /* SDI (not used here)                  */
        6,               /* receive buffer size of 6 (see above) */
        ax_LData,        /* initialization data                  */
        API429_FALSE);
    AIM_CHECK_RESULT_RETURN(retVal);

    return retVal;
}

AiReturn SetupRx(AiUInt8 boardHandle, AiUInt8 channelId)
{
    AiReturn  retVal = 0;
    AiUInt32  ul_LabIndex;

    printf("Setting up Rx channel\r\n");

    /*retVal = Api429ChannelSpeedSet( boardHandle, channelId, API429_HI_SPEED ); // calling this function is not mandatory, use default instead */

    /* Intialize the receiver to normal mode and use MSB as parity bit */
    retVal = Api429RxInit(boardHandle, channelId, AiFalse, AiFalse);
    AIM_CHECK_RESULT_RETURN(retVal);

    /* Note: to enable SDI sorting, the bitfield x_Cmd.sm of Api429BoardReset has to be set. */

    /* loop over all labels 0..255 --> 256 possible lables */
    for (ul_LabIndex = 0; ul_LabIndex <= 255; ul_LabIndex++)
    {
        SetupLabel(boardHandle, channelId, ul_LabIndex);
        AIM_CHECK_RESULT_RETURN(retVal);
    }

    return retVal;
}

AiReturn ReadRxData(AiUInt8 boardHandle, AiUInt8 channelId)
{
    AiUInt32 ul_LabIndex = 0;
    AiReturn retVal = 0;

    /* Api429RxLabelBufferOffsetGet */
    AiUInt32 l_Addr;
    AiUInt16 w_Size;
    /* Api429BoardMemBlockRead */
    AiUInt32 aul_Data[7];
    AiUInt32 ul_BytesRead;

    /* Api429RxLabelBufferRead */
    struct api429_rx_buf_entry ax_LData[6];
    struct api429_rx_buf_ctl   x_Ctl;

    printf("Read RX data (for simplicity, only label 0-5 are checked)\r\n");


    /* This is the first method to read a RX buffer
     * It can quickly be integrated, but should not be used within an interrupt service routine
     * The latest entry can be found at ax_LData[0].lab_data */
    printf("  using API command\r\n");
    for (ul_LabIndex = 0; ul_LabIndex <= 5; ul_LabIndex++)
    {
        retVal = Api429RxLabelBufferRead(boardHandle, channelId, (AiUInt8)ul_LabIndex, 0/*no SDI*/, 6, &x_Ctl, ax_LData);
        AIM_CHECK_RESULT_RETURN(retVal);

        printf("   Label %d Buffer: %08x %08x %08x %08x %08x %08x\r\n", ul_LabIndex, ax_LData[0].lab_data, ax_LData[1].lab_data, ax_LData[2].lab_data, ax_LData[3].lab_data, ax_LData[4].lab_data, ax_LData[5].lab_data);
    }

    /* This is the second method to read a RX buffer
     * It's a little bit more complicated, but faster
     * The latest entry can be found at aul_Data[CI+1] */
    printf("  using direct memory read\r\n");
    for (ul_LabIndex = 0; ul_LabIndex <= 5; ul_LabIndex++)
    {
        retVal = Api429RxLabelBufferOffsetGet(boardHandle, channelId, (AiUInt8)ul_LabIndex, 0/*no SDI*/, &l_Addr, &w_Size);
        AIM_CHECK_RESULT_RETURN(retVal);

        retVal = Api429BoardMemBlockRead(boardHandle, AI_MEMTYPE_GLOBAL, l_Addr, 4, (void *)aul_Data, 7, &ul_BytesRead);
        AIM_CHECK_RESULT_RETURN(retVal);

        printf("   Label %d Buffer: [CI:%x] %08x %08x %08x %08x %08x %08x\r\n", ul_LabIndex, (aul_Data[0] & 0x3ff), aul_Data[1], aul_Data[2], aul_Data[3], aul_Data[4], aul_Data[5], aul_Data[6]);
    }

    return retVal;
}

/* EOF */

