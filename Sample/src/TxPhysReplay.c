/* SPDX-FileCopyrightText: 2001-2021 AIM GmbH <info@aim-online.com> */
/* SPDX-License-Identifier: MIT */

#include "A429_sample.h"

#define RP_FILE_NAME "replay.arc"


/* ######################################################################################*/
/*                                                                                       */
/*                                     Sample entry point                                */
/*                                                                                       */
/* ######################################################################################*/
AiReturn TxPhysicalReplaySample(AiUInt8 boardHandle, AiUInt8 channelId)
{
    AiReturn retVal = 0;

    /* Api429BoardInfoGet*/
    struct api429_board_info board_info;
    AiUInt32 emb_flag = 0;

    /* Api429TxStatusGet */
    AiUInt8  b_TxStatus;
    AiUInt32 l_GlbCnt;

    /* Api429ReplayStatusGet */
    struct api429_replay_status x_RepStatus;


    FILE *px_ReplayFile;
    AiUInt32 ul_FileSize = 0;
    AiUInt32 ul_fileGotSize = 0;
    void    *p_DataBuffer = NULL;

    /* For handling the Replay interrupt counts */
    AiInt32  ul_RpiDiff, ul_OldRpiCnt;

    AiUInt32 ul_BlockSize;
    AiUInt32 ul_BytesWritten;




    printf("Running TxPhysicalReplaySample \r\n");
    printf("This allows to replay a given recording file.\r\n");

    retVal = Api429BoardInfoGet(boardHandle, &board_info);
    AIM_CHECK_RESULT_RETURN(retVal);
    emb_flag = API429_BOARD_TYPE_IS_EMBEDDED(&board_info);
    if (emb_flag == AiTrue)
    {
        printf("\r\n*** Sample terminated: Replay Function is not supported on Embedded Board types !\r\n");
        return -1;
    }

    retVal = Api429TxInit(boardHandle, channelId, API429_TX_MODE_PHYS_REPLAY, 0);
    AIM_CHECK_RESULT_RETURN(retVal);




    // open Replay File
    printf("\n Open File %s to replay\n", RP_FILE_NAME);
    px_ReplayFile = fopen(RP_FILE_NAME, "rb");
    if (px_ReplayFile == NULL)
    {
        printf("\r\n*** Sample terminated: unable to open replay file\r\n");
        return -1;
    }


    AiOsGetFileSize(RP_FILE_NAME, &ul_FileSize);

    retVal = Api429ReplayInit(boardHandle,
        channelId,
        0,				// uc_ClrEntryBit,
        0,				// uc_NoRepCnt, 
        1,				// uc_CycOpr,       --> cyclically replay
        1,				// uc_RepErrors,    --> also replay errors
        0x01,			// uc_RepIntMode,   --> raise interrupts on half buffers
        0,				// uc_AbsLongTTag,
        0,				// uw_DayOfYear, 
        0,				// ul_Min, 
        0,				// ul_MSec, 
        ul_FileSize);
    AIM_CHECK_RESULT_RETURN(retVal);



    // Status Read to get Size and load address
    retVal = Api429ReplayStatusGet(boardHandle, channelId, &x_RepStatus);
    AIM_CHECK_RESULT_RETURN(retVal);

    ul_BlockSize = x_RepStatus.ul_Size;
    p_DataBuffer = malloc(ul_BlockSize);

    if (NULL == p_DataBuffer)
    {
        printf("Unable to allocate buffer\r\n");
        return -1;
    }



    // load first Half Buffer
    ul_fileGotSize = fread(p_DataBuffer, 1/*Byte*/, ul_BlockSize, px_ReplayFile);
    x_RepStatus.ul_Size = ul_fileGotSize;

    retVal = Api429ReplayDataWrite(boardHandle, channelId, &x_RepStatus, p_DataBuffer, &ul_BytesWritten);
    AIM_CHECK_RESULT_RETURN(retVal);



    // Status Read to the next load address
    retVal = Api429ReplayStatusGet(boardHandle, channelId, &x_RepStatus);
    AIM_CHECK_RESULT_RETURN(retVal);

    // load second Half Buffer
    ul_fileGotSize = fread(p_DataBuffer, 1 /*Byte*/, ul_BlockSize, px_ReplayFile);
    x_RepStatus.ul_Size = ul_fileGotSize;

    retVal = (AiUInt8)Api429ReplayDataWrite(boardHandle, channelId, &x_RepStatus, p_DataBuffer, &ul_BytesWritten);
    AIM_CHECK_RESULT_RETURN(retVal);





    /* Start the replay */
    Api429ChannelStart(boardHandle, channelId);
    AIM_CHECK_RESULT_RETURN(retVal);


    /* Read initial value for ul_OldRpiCnt */
    retVal = Api429ReplayStatusGet(boardHandle, channelId, &x_RepStatus);
    AIM_CHECK_RESULT_RETURN(retVal);
    ul_OldRpiCnt = x_RepStatus.ul_RpiCnt;


    while ((API429_BUSY == x_RepStatus.uc_Status))
    {
        ul_RpiDiff = x_RepStatus.ul_RpiCnt - ul_OldRpiCnt;

        /* reload, if needed. */
        if (0 != ul_RpiDiff)
        {
            if (1 < ul_RpiDiff)
            {
                printf("  >>> Replay Error <<<\n");
                break;
            }

            ul_fileGotSize = fread(p_DataBuffer, 1 /*Byte*/, ul_BlockSize, px_ReplayFile);
            if (ul_fileGotSize < ul_BlockSize)
            {
                x_RepStatus.ul_Size = ul_fileGotSize;
            }

            retVal = Api429ReplayDataWrite(boardHandle, channelId, &x_RepStatus, p_DataBuffer, &ul_BytesWritten);
            AIM_CHECK_RESULT_RETURN(retVal);

            ul_OldRpiCnt = x_RepStatus.ul_RpiCnt;
        }

        AiOsSleep(100);



        /* Write some status information */
        retVal = Api429TxStatusGet(boardHandle, channelId, &b_TxStatus, &l_GlbCnt);
        AIM_CHECK_RESULT_RETURN(retVal);

        retVal = Api429ReplayStatusGet(boardHandle, channelId, &x_RepStatus);
        AIM_CHECK_RESULT_RETURN(retVal);

        printf(" Tx Status = %d; Rep Status = %d; HFI = %d; Count = %d \r",
            b_TxStatus, x_RepStatus.uc_Status, x_RepStatus.ul_RpiCnt, l_GlbCnt);
    } // End While





    /* Print a final status information */
    Api429TxStatusGet(boardHandle, channelId, &b_TxStatus, &l_GlbCnt);
    AIM_CHECK_RESULT_RETURN(retVal);

    printf("\n Tx Status = %d; Rep Status = %d; HFI = %d; Count = %d \r", b_TxStatus, x_RepStatus.uc_Status, x_RepStatus.ul_RpiCnt, l_GlbCnt);


    /* Clean up */
    if (NULL == p_DataBuffer)
        free(p_DataBuffer);

    fclose(px_ReplayFile);



    return retVal;
}
