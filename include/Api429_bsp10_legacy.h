// SPDX-FileCopyrightText: 2016-2021 AIM GmbH <info@aim-online.com>
// SPDX-License-Identifier: MIT

/*! \file Api429_bsp10_legacy.h
 *
 *  This header file contains functions and definitions which
 *  help users of BSPs before 10.0 to compile their old applications.
 *
 *  This functions must not be used for new applications.
 *
 *  For the documentation of the functions please refer the  
 *  reference manual of any 9.xx BSP, which was used to the application.
*/

#ifndef API429_BSP10_LEGACY_H_
#define API429_BSP10_LEGACY_H_


#include "Api429.h"
#include "Api429BoardDriver.h"
#include "Api429Update.h"

#include <string.h>
#include <stdlib.h>


#define API429_MODULE_1   0         /* destination code first API*/
#define API429_MODULE_2   1         /* destination code second API if installed */
#define API429_MODULE_3   2         /* destination code third API if installed  */
#define API429_MODULE_4   3         /* destination code fourth API if installed */
#define API429_MODULE_5   4         /* destination code fifth API if installed */
#define API429_MODULE_6   5         /* destination code sixth API if installed */
#define API429_MODULE_7   6         /* destination code seventh API if installed */
#define API429_MODULE_8   7         /* destination code eighth API if installed */
#define API429_MODULE_9   8
#define API429_MODULE_10  9
#define API429_MODULE_11 10
#define API429_MODULE_12 11
#define API429_MODULE_13 12
#define API429_MODULE_14 13
#define API429_MODULE_15 14
#define API429_MODULE_16 15
#define API429_MODULE_17 16
#define API429_MODULE_18 17
#define API429_MODULE_19 18
#define API429_MODULE_20 19
#define API429_MODULE_21 20
#define API429_MODULE_22 21
#define API429_MODULE_23 22
#define API429_MODULE_24 23
#define API429_MODULE_25 24
#define API429_MODULE_26 25
#define API429_MODULE_27 26
#define API429_MODULE_28 27
#define API429_MODULE_29 28
#define API429_MODULE_30 29
#define API429_MODULE_31 30
#define API429_MODULE_32 31

#define MAX_API429_CHN 32

#define API_MEMTYPE_GLOBAL          0
#define API_MEMTYPE_SHARED          1
#define API_MEMTYPE_LOCAL           2
#define API_MEMTYPE_IO              3
#define API_MEMTYPE_GLOBAL_UNCACHED 4

/* function Api429GetMemLocation */
#define API429_GETMEM_XFER               1
#define API429_GETMEM_XFER_BUF           2
#define API429_GETMEM_MINFRM             3
#define API429_GETMEM_MAJFRM             4
#define API429_GETMEM_LABDESC            5
#define API429_GETMEM_RXBUF              6
#define API429_GETMEM_TXCOUNT            7
#define API429_GETMEM_CHN_DESC           8

/* function Api429ReadBSPVersion(Ex) */
#define API429_BSP_COMPATIBLE            0x00
#define API429_BSP_NOT_COMPATIBLE        0xFF

/* Api429CmdTxIni */
#define API429_TX_MODE_FRAMING           0     /* standard framing mode */
#define API429_TX_MODE_POLLUT            1     /* polluton mode */
#define API429_TX_MODE_REPLAY            0xFF     /* replay mode --> no longer used*/
#define API429_TX_MODE_PHYS_REPLAY       2     /* physical replay mode */
#define API429_TX_MODE_FRAMING_DYNTAG    3     /* standard framing mode with dyntags */


/* defines for receiver */
#define API429_RX_MODE_NORM              0     /* normal receive mode */
#define API429_RX_MODE_LOOP              1     /* loop mode */


/* defines for receiver monitor */
#define API429_TAT_REC                   0     /* continous capturing */
#define API429_MON_MODE_LOC              0     /* local monitoring */
#define API429_MON_MODE_GLB              1     /* global monitoring */
#define API429_SPT_STP                   0     /* API capture start pointer */
#define API429_SPT_CTP                   1     /* API trigger pointer */
#define API429_SPT_ETP                   2     /* API capture end pointer */


/* Api429CmdReset */
#define API429_FULL_RESET                0     /* Complete Reset */


/* Monitor Capture Mode */
#define API429_MON_CAP_STANDARD          0
#define API429_MON_CAP_RECORDING         1


/* defines for trigger */
#define API429_TRG_START      0   /* trigger on start function */
#define API429_TRG_ERR        1   /* trigger on any error */
#define API429_TRG_EXT        2   /* trigger on external input */
#define API429_TRG_ANY        3   /* trigger on first event */

#define API429_TRG_IR_DIS     0   /* disable trigger interrupt */
#define API429_TRG_IR_START   1   /* enable interrupt on START trigger */
#define API429_TRG_IR_STOP    2   /* enable interrupt on STOP trigger */
#define API429_TRG_IR_BFI     3   /* enable interrupt on buffer full */
#define API429_TRG_IR_HFI     4   /* enable interrupt on half buffer full */

#define API429_TRG_ACT_DIS    0    /* disable extended action on trigger */

/* Api429CmdTxStart */
#define API429_TX_MAJFRAME              0     /* execute major frame */
#define API429_TX_ACYCLIC               1     /* execute acyclic instruction list */
#define API429_TX_POLLUT                2     /* execute pollution mode */
#define API429_TX_REPLAY                3     /* execute replay mode */
#define API429_TX_PHYS_REPLAY           4     /* execute physical replay mode */

#define API429_TX_MAJFRAME_CYCLIC       0     /* excute major frame cyclic */
#define API429_TX_REPLAY_CYCLIC         0     /* excute replay mode cyclic */

#define API429_TX_START_IMM             0     /* start transmission immediately */
#define API429_TX_START_EXT             1     /* start transmission via ext. trigger */
#define API429_TX_START_TTG             2     /* start transmission on timetag */
#define API429_TX_START_NO_WFMFT        3     /* start transmission immediately, skipping the first 
                                               * wait-for-minor-frame-time firmware instruction */
#define API429_TX_START_PREPARE_FRAMING 4     /* do not start, but create framing (if tx channel 
                                               * was initialized with API429_TX_MODE_AUTO_FRAME */



#define API429_PXI_SET_TTSRC_EXTERNAL    2  /* obsolete, replaced by API429_PXI_SET_TTSRC_BACKPLANE */
#define API429_PXI_SET_TTSRC_INTERNAL    3  /* obsolete, replaced by API429_PXI_SET_TTSRC_FRONT */


/* fifo functions */
#define API429_FIFO_WRITE_DEFAULT          0
#define API429_FIFO_WRITE_BLOCKING         1


/* Define Receiver Monitor Capture Mode Command structure */
typedef struct
{
    AiUInt8  mode;
    AiUInt8  padding1;
    AiUInt16  padding2;
    AiUInt16 tat;
    AiUInt16 reserved;
    AiUInt32 rec_filesize;
} TY_API429_RCV_CAP_MODE_CMD;

/* Define Receiver Monitor Trigger Parameter Command structure */
typedef struct
{
    AiUInt8 start_pat;
    AiUInt8 start_mask;
    AiUInt8 stop_pat;
    AiUInt8 stop_mask;
} TY_API429_TRG_PAR_CMD;

/* Define Receiver Monitor Function Block Command structure */
typedef struct
{
    AiUInt8 fb_id;
    AiUInt8 fbi;
    AiUInt8 ir;
    AiUInt8 ulc;
    AiUInt8 uli;
    AiUInt8 llc;
    AiUInt8 lli;
    AiUInt8 fe;
    AiUInt32 trg_set;
    AiUInt32 trg_reset;
    AiUInt16 pre_cnt;
    AiUInt16 pre_rel;
    AiUInt32 mask;
    AiUInt32 ulim;
    AiUInt32 llim;
    AiUInt8  ext_trg;
    AiUInt8  trg_line;
} TY_API429_RCV_FB_CMD;


typedef struct
{
    AiUInt32 ul_NrOfChannels;
    AiUInt32 ul_DiscreteSetup;
} TY_API429_DISCR_CONFIG_GET;

typedef struct
{
    AiUInt32 l_StartMode;
    AiUInt32 l_Mode;
    AiUInt32 l_ModeSpec;
    AiUInt32 l_TrgLine;
    AiUInt32 l_TTDay;
    AiUInt32 l_TTHour;
    AiUInt32 l_TTMinute;
    AiUInt32 l_TTSec;
    AiUInt32 l_TTuSec;
} TY_API429_TXSTART;


#define API429_RESET_ALL                  0
#define API429_RESET_READ                 1

/* Reset Command structure */
typedef struct
{
    AiUInt8 res_ctl;
    AiUInt8 glob_speed_mod;
    AiUInt8 low_speed_sel;
    AiUInt8 tx_clock_sel;
    AiUInt32 sm;
    AiUInt32 lcen;
    AiUInt32 mbuf_size;
    AiUInt32 chn_cfg;
} TY_API429_RESET_CMD;

/* Reset Command Acknowledge structure */
typedef struct
{
    AiUInt16 chn_cnt;
    AiUInt16 padding1;
    AiUInt32 pchc;
    AiUInt32 txrx_mem_start;
    AiUInt32 txrx_mem_end;
    AiUInt32 res1;
    AiUInt32 res2;
    AiUInt32 rm_mem_start[API429_MAX_CHANNELS];
    AiUInt32 rm_mem_size[API429_MAX_CHANNELS];
} TY_API429_RESET_ACK;

typedef struct api429_device_config
{
    AiUInt32 ul_DmaEnabled;
    AiUInt32 ul_DmaMinimumSize;
    AiUInt32 ul_ReservedLW2;
    AiUInt32 ul_ReservedLW3;
    AiUInt32 ul_ReservedLW4;
    AiUInt32 ul_ReservedLW5;
    AiUInt32 ul_ReservedLW6;
    AiUInt32 ul_ReservedLW7;
    AiUInt32 ul_ReservedLW8;
} TY_API429_DEVICE_CONFIG;

typedef struct
{
    AiUInt32 ul_DiscreteChannelId; /* Discrete to send the PPS signal to */
    AiUInt32 ul_EnaDis; /* if set to '1' enable, if set to '0' disable */
} TY_API429_DISCR_PPS;


typedef struct
{
    AiUInt8  uc_Status;
    AiUInt8  uc_Padding1;
    AiUInt16 uw_Padding2;
    AiUInt32 ul_RpiCnt;
    AiUInt32 ul_EntryCnt;
    AiUInt32 ul_StartAddr;
    AiUInt32 ul_Size;
} TY_API429_REP_STATUS;


typedef struct
{
    AiUInt8 tmod;
    AiUInt8 exti;
    AiUInt8 imod;
    AiUInt8 ext;
    AiUInt8 exto;
    AiUInt8 exta;
    AiUInt8 extad;
    AiUInt8 reserved;
} TY_API429_RCV_TRG_MODE_CMD;


/* Label transfer structure */
typedef struct
{
    AiUInt32 xfer_id;
    AiUInt8  xfer_type;
    AiUInt8  xfer_ir;
    AiUInt16 padding1;
    AiUInt16 ir_index;
    AiUInt16 buf_size;
    AiUInt8  err_type;
    AiUInt8  err_type_ext;
    AiUInt16 xfer_gap;
    AiUInt8  extTrigger;
} TY_API429_LXFER_CMD;

typedef struct
{
    AiUInt32  ul_Status;
    AiUInt32  ul_FreeMem;
    AiUInt32  ul_XferDescAddr;
} TY_API429_LXFER_OUT;


typedef struct api429_server_info
{
    AiChar szNBComputerName[128]; //!< NetBios related Computer Name
    AiChar szApplicationName[128]; //!< Application name of the ANS software (AIM Network Server), if available
    AiChar szCompanyName[64]; //!< Reserved
    AiChar szCopyright[64]; //!< Reserved
    AiChar szDescription[128]; //!< Description
    AiChar szBuiltDate[64]; //!< Reserved
    AiUInt16 usMajorVersion; //!< Major Version of the ANS server, if available
    AiUInt16 usMinorVersion; //!< Minor Version of the ANS server, if available
    AiUInt uiBuiltNumber;  //!< Reserved
    AiChar szIFUuid[128]; //!< Reserved
    AiUInt16 usIFMajorVersion; //!< Major Version of the internal network interface
    AiUInt16 usIFMinorVersion; //!< Minor Version of the internal network interface
    AiUInt8 bSrvOSVersion; //!< Reserved
} TY_API429_SERVERINFO;


#define API_IRIG_SET 0 
#define API_IRIG_CHANGE_TIMETAG_SOURCE 1

typedef struct
{
    AiUInt8 b_DayHi;
    AiUInt8 b_DayLo;
    AiUInt8 b_Hour;
    AiUInt8 b_Min;
    AiUInt8 b_Sec;
    AiUInt8 b_MsHi;
    AiUInt8 b_MsLo;
    AiUInt8 b_mode;
    AiUInt8 b_irig_src;
    AiUInt8 b_irig_sync;
} TY_API429_IRIG_TIME;


typedef void (_API_ISR_FUNC *TY_INT429_FUNC_PTR)(AiUInt8 uc_Module, AiUInt8 uc_Chn, AiUInt8 uc_Type, TY_API429_INTR_LOGLIST_ENTRY x_Info);



AI_LIB_FUNC AiUInt8 AI_CALL_CONV Api429CmdGetIrigTime(AiUInt8 b_Module, TY_API429_IRIG_TIME *px_IrigTime);
AI_LIB_FUNC AiUInt8 AI_CALL_CONV Api429CmdSetIrigTime(AiUInt8 b_Module, TY_API429_IRIG_TIME *px_IrigTime);
AI_LIB_FUNC AiReturn AI_CALL_CONV Api429CmdTxStartEx(AiUInt8 b_Module, AiUInt8 b_Chn, TY_API429_TXSTART *px_TxStartIn);
AI_LIB_FUNC AiUInt8 AI_CALL_CONV Api429CmdTxStart(AiUInt8 b_Module, AiUInt8 b_Chn, AiUInt8 b_StartMode, AiUInt8 b_Mode, AiUInt16 w_ModeSpec, AiUInt8 b_TrgLine);
AI_LIB_FUNC const char* AI_CALL_CONV Api429GetErrorDescription(AiReturn ul_ErrorCode);
AI_LIB_FUNC AiUInt8 AI_CALL_CONV Api429Init(void);
AI_LIB_FUNC AiUInt8 AI_CALL_CONV Api429Exit(void);
AI_LIB_FUNC short AI_CALL_CONV Api429SetDllDbgLevel(AiUInt32 ul_DllDbgLevel);
AI_LIB_FUNC void AI_CALL_CONV Api429SetPnpCallback(void* PnPCallbackFunction);
AI_LIB_FUNC AiUInt8 AI_CALL_CONV Api429ConnectToServer(const char * pszNetworkAdress, AiInt16 *w_ServerBoards);
AI_LIB_FUNC AiInt16 AI_CALL_CONV Api429DisconnectFromServer(const char * pszNetworkAdress);
AI_LIB_FUNC const char* AI_CALL_CONV Api429GetBoardName(AiUInt8 b_Module);
AI_LIB_FUNC AiUInt8 AI_CALL_CONV Api429CmdReset(AiUInt8 b_Module, TY_API429_RESET_CMD  *px_Cmd, TY_API429_RESET_ACK * px_Ackn);
AI_LIB_FUNC short AI_CALL_CONV Api429Open(AiUInt8 b_Module, const char *ac_SrvName, AiUInt8 *bModuleHandle);
AI_LIB_FUNC AiUInt8 AI_CALL_CONV Api429Close(AiUInt8 b_Module);
AI_LIB_FUNC AiInt16 AI_CALL_CONV Api429SetDeviceConfig(AiUInt8 b_Module, TY_API429_DEVICE_CONFIG * pxConfig);
AI_LIB_FUNC AiInt16 AI_CALL_CONV Api429SetDeviceConfigDefault(AiUInt8 b_Module);
AI_LIB_FUNC AiInt16 AI_CALL_CONV Api429GetDeviceConfig(AiUInt8 b_Module, TY_API429_DEVICE_CONFIG * pxConfig);
AI_LIB_FUNC AiUInt8 AI_CALL_CONV Api429CmdBite(AiUInt8 b_Module, AiUInt8 b_Func, AiUInt8 *pb_Status, AiUInt8  *pb_Echo);
AI_LIB_FUNC AiUInt8 AI_CALL_CONV Api429CmdChannelWrapControl(AiUInt8 uc_Module, AiUInt32 ul_Control);
AI_LIB_FUNC AiUInt8 AI_CALL_CONV Api429CmdChannelWrapGetConnection(AiUInt8 uc_Module, AiUInt8 auc_Connection[API429_MAX_CHANNELS + 1]);
AI_LIB_FUNC AiUInt8 AI_CALL_CONV Api429CmdChannelWrapIsSupported(AiUInt8 uc_Module, AiUInt8 *pucResult);
AI_LIB_FUNC AiInt16 AI_CALL_CONV Api429GetDriverInfo(AiUInt8 uc_Module, TY_429_API_DRIVER_INFO * pxDriverInfo);

typedef struct
{
    AiUInt32 ul_Mode;
    AiUInt32 ul_TrgSource;
    AiUInt32 ul_TrgDest;
    AiUInt32 ul_TTClear;
} TY_API429_PXI_CON;

AI_LIB_FUNC AiUInt8 AI_CALL_CONV Api429CmdSysPXICon(AiUInt8 uc_Module, TY_API429_PXI_CON *px_PxiCon);
AI_LIB_FUNC AiInt16 AI_CALL_CONV Api429GetServerInfo(AiUInt8 bModule, TY_API429_SERVERINFO *pServerInfo);
AI_LIB_FUNC short AI_CALL_CONV Api429GetTgEmul(AiUInt8 bModule);
AI_LIB_FUNC void AI_CALL_CONV Api429SetTgEmul(AiUInt8 bModule, short mode);
AI_LIB_FUNC AiUInt8 AI_CALL_CONV Api429CmdGetMemLocation(AiUInt8 uc_Module, AiUInt8 uc_Chn, AiUInt32 ul_Mode, AiUInt32 ul_Id, AiUInt32 *pul_MemType, AiUInt32 *pul_Offset);
AI_LIB_FUNC AiUInt8 AI_CALL_CONV Api429GetTimerAddr(AiUInt8 uc_Module, AiUInt32 *pul_TimerOffset, AiUInt32 *pul_MemType);
AI_LIB_FUNC AiUInt8 AI_CALL_CONV Api429ReadMemData(AiUInt8 bModule, AiUInt8 memtype, AiUInt32 offset, AiUInt8 width, void* data_p);
AI_LIB_FUNC AiUInt8 AI_CALL_CONV Api429WriteMemData(AiUInt8 bModule, AiUInt8 memtype, AiUInt32 offset, AiUInt8 width, void* data_p);
AI_LIB_FUNC AiUInt8 AI_CALL_CONV Api429ReadBlockMemData(AiUInt8 bModule, AiUInt8 memtype, AiUInt32 offset, AiUInt8 width, void* data_p, AiUInt32 size,AiUInt32 *pul_BytesRead);
AI_LIB_FUNC AiUInt8 AI_CALL_CONV Api429WriteBlockMemData(AiUInt8 bModule, AiUInt8 memtype, AiUInt32 offset, AiUInt8 width, void* data_p, AiUInt32 size,AiUInt32 *pul_BytesWritten);
AI_LIB_FUNC AiUInt8 AI_CALL_CONV Api429CmdDefChnSpeed(AiUInt8 b_Module, AiUInt8 b_Chn, AiUInt8 b_Speed);
AI_LIB_FUNC short AI_CALL_CONV Api429InstIntHandler(AiUInt8 b_Module, AiUInt8 uc_Chn, AiUInt8 uc_Type, TY_INT429_FUNC_PTR pf_IntFunc);
AI_LIB_FUNC short AI_CALL_CONV Api429DelIntHandler(AiUInt8 b_Module, AiUInt8 uc_Chn, AiUInt8 uc_Type);
AI_LIB_FUNC AiUInt8 AI_CALL_CONV Api429CmdReadDiscretes(AiUInt8 uc_Module, AiUInt32 *pul_Value);
AI_LIB_FUNC AiUInt8 AI_CALL_CONV Api429CmdWriteDiscretes(AiUInt8 uc_Module, AiUInt32 ul_Mask, AiUInt32 ul_Value);
AI_LIB_FUNC AiUInt8 AI_CALL_CONV Api429CmdInitDiscretes(AiUInt8 uc_Module, AiUInt32 ul_DiscreteSetup);
AI_LIB_FUNC AiUInt8 AI_CALL_CONV Api429CmdReadDiscreteConfig(AiUInt8 uc_Module, TY_API429_DISCR_CONFIG_GET *px_DiscrConfig);
AI_LIB_FUNC AiUInt8 AI_CALL_CONV Api429CmdDiscrPPSCntrl(AiUInt8 uc_Module, TY_API429_DISCR_PPS *px_Pps);
AI_LIB_FUNC AiUInt8 AI_CALL_CONV Api429CmdReplayIni(AiUInt8 b_Module, AiUInt8 uc_Chn, AiUInt8 uc_ClrEntryBit, AiUInt8 uc_NoRepCnt, AiUInt8 uc_CycOpr, AiUInt8 uc_RepErrors, AiUInt8 uc_RepIntMode,AiUInt8 uc_AbsLongTTag, AiUInt16 uw_DayOfYear, AiInt32 l_Min, AiInt32 l_MSec, AiUInt32 ul_FileSize);
AI_LIB_FUNC AiUInt8 AI_CALL_CONV Api429CmdReplayStatus(AiUInt8 b_Module, AiUInt8 uc_Chn, TY_API429_REP_STATUS *px_RepStatus);
AI_LIB_FUNC short AI_CALL_CONV Api429WriteRepData(AiUInt8 bModule, AiUInt8 uc_Chn, TY_API429_REP_STATUS *px_RepStatus, void *pv_Buf, AiUInt32 *pul_BytesWritten);
AI_LIB_FUNC AiUInt8 AI_CALL_CONV Api429CmdRmCapDef(AiUInt8 b_Module, AiUInt8 b_Chn, TY_API429_RCV_CAP_MODE_CMD *px_CapMode);
AI_LIB_FUNC AiUInt8 AI_CALL_CONV Api429CmdRmIni(AiUInt8 b_Module, AiUInt8 b_Chn, AiUInt8 mode);
AI_LIB_FUNC AiUInt8 AI_CALL_CONV Api429CmdRmTrgDef(AiUInt8 b_Module, AiUInt8 b_Chn, TY_API429_RCV_TRG_MODE_CMD* px_TrgMode);
AI_LIB_FUNC AiUInt8 AI_CALL_CONV Api429CmdRmLabCon(AiUInt8 b_Module, AiUInt8 b_Chn, AiUInt8 b_Label, AiUInt8 b_Sdi, AiUInt8 b_Con);
AI_LIB_FUNC AiUInt8 AI_CALL_CONV Api429CmdRmTrgStartDef(AiUInt8 b_Module, AiUInt8 b_Chn, AiUInt8 b_Label, AiUInt8 b_Sdi, AiUInt8 b_Con, TY_API429_TRG_CTL_CMD  *px_Ctl);
AI_LIB_FUNC AiUInt8 AI_CALL_CONV Api429CmdRmTrgStopDef(AiUInt8 b_Module, AiUInt8 b_Chn, AiUInt8 b_Label, AiUInt8 b_Sdi, AiUInt8 b_Con, TY_API429_TRG_CTL_CMD  *px_Ctl);
AI_LIB_FUNC AiUInt8 AI_CALL_CONV Api429CmdRmStart(AiUInt8 b_Module, AiUInt8 b_Chn);
AI_LIB_FUNC AiUInt8 AI_CALL_CONV Api429CmdRmHalt(AiUInt8 b_Module, AiUInt8 b_Chn);
AI_LIB_FUNC AiUInt8 AI_CALL_CONV Api429CmdRmStatusRead(AiUInt8 b_Module, AiUInt8 b_Chn, AiUInt8  *b_RmStatus, AiUInt16  *pw_Msw);
AI_LIB_FUNC AiUInt8 AI_CALL_CONV Api429CmdRmStkPtrRead(AiUInt8 b_Module, AiUInt8 b_Chn, AiUInt32  *pl_Stp, AiUInt32  *pl_Ctp, AiUInt32  *pl_Etp);
AI_LIB_FUNC AiUInt8 AI_CALL_CONV Api429CmdRmDataRead(AiUInt8 b_Module, AiUInt8 b_Chn, AiUInt16 uw_EntriesToRead, AiUInt16 *puw_Count,TY_API429_RCV_STACK_ENTRY *px_SData);
AI_LIB_FUNC AiUInt8 AI_CALL_CONV Api429CmdRmDataReadEx(AiUInt8 b_Module, AiUInt8 b_Chn, AiUInt16 uw_EntriesToRead, AiUInt16 *puw_Count,TY_API429_RCV_STACK_ENTRY_EX *px_SData);
AI_LIB_FUNC AiUInt8 AI_CALL_CONV Api429CmdRxIni(AiUInt8 b_Module, AiUInt8 b_Chn, AiUInt8 b_Mode, AiUInt8 b_ModeExt, AiUInt8 b_Parity);
AI_LIB_FUNC AiUInt8 AI_CALL_CONV Api429RxReadActivity(AiUInt8 b_Module, TY_API429_RX_ACTIVITY *pxActivity);
AI_LIB_FUNC AiUInt8 AI_CALL_CONV Api429CmdRxLabCon(AiUInt8 b_Module, AiUInt8 b_Chn, AiUInt8 b_Label, AiUInt8 b_Sdi, AiUInt8 b_Con, AiUInt8 b_IrCon,AiUInt16 w_IrIndex, AiUInt16 w_BufSize, AiUInt8 *puc_Status, AiUInt32 *pul_FreeMem);
AI_LIB_FUNC AiUInt8 AI_CALL_CONV Api429CmdRxLabDataCon(AiUInt8 b_Module, AiUInt8 b_Chn, AiUInt8 b_Label, AiUInt8 b_Sdi, AiUInt16 w_BufSize, TY_API429_RX_BUF_ENTRY  *px_LData, AiUInt8 b_Clear);
AI_LIB_FUNC AiUInt8 AI_CALL_CONV Api429CmdRxStatusRead(AiUInt8 b_Module, AiUInt8 b_Chn, AiUInt8  *b_RxStatus, AiUInt32  *px_MsgCnt, AiUInt32  *px_ErrCnt);
AI_LIB_FUNC AiUInt8 AI_CALL_CONV Api429CmdRxLabRead(AiUInt8 b_Module, AiUInt8 b_Chn, AiUInt8 b_Label, AiUInt8 b_Sdi, AiUInt8 b_LabCntIni, AiUInt16  *pw_LabIx,AiUInt32  *px_LabCnt, AiUInt32  *px_LabErr);
AI_LIB_FUNC AiUInt8 AI_CALL_CONV Api429CmdRxLabDataRead(AiUInt8 b_Module, AiUInt8 b_Chn, AiUInt8 b_Label, AiUInt8 b_Sdi, AiUInt16 w_BufSize,TY_API429_RX_BUF_CTL  *px_Info, TY_API429_RX_BUF_ENTRY  *px_LData);
AI_LIB_FUNC AiUInt8 AI_CALL_CONV Api429CmdRxGetBufAddress(AiUInt8 b_Module, AiUInt8 b_Chn, AiUInt8 b_Label, AiUInt8 b_Sdi, AiUInt32  *px_Addr, AiUInt16  *w_Size);
AI_LIB_FUNC AiUInt8 AI_CALL_CONV Api429CmdRxPollBlkDef(AiUInt8 b_Module, AiUInt8 b_Chn, AiUInt8 b_Label, AiUInt8 b_Sdi, AiUInt8 b_Con, TY_API429_RCV_PB_CMD  *px_PollBlk);
AI_LIB_FUNC AiUInt8 AI_CALL_CONV Api429CmdRxStart(AiUInt8 b_Module, AiUInt8 b_Chn);
AI_LIB_FUNC AiUInt8 AI_CALL_CONV Api429CmdRxHalt(AiUInt8 b_Module, AiUInt8 b_Chn);
AI_LIB_FUNC AiUInt8 AI_CALL_CONV Api429CmdTxIni(AiUInt8 b_Module, AiUInt8 b_Chn, AiUInt8 b_Mode, AiUInt8 b_Parity);
AI_LIB_FUNC AiUInt8 AI_CALL_CONV Api429CmdTxAmpIniEx(AiUInt8 b_Module, AiUInt8 b_Chn, AiFloat fAmplitude, AiFloat *pfCalcAmpl);
AI_LIB_FUNC AiUInt8 AI_CALL_CONV Api429CmdTxStatusRead(AiUInt8 b_Module, AiUInt8 b_Chn, AiUInt8  *pb_TxStatus, AiUInt32  *pl_GlbCnt, AiUInt16  *pw_RelFlag);
AI_LIB_FUNC AiUInt8 AI_CALL_CONV Api429CmdTxFrmTimeIniEx(AiUInt8 b_Module, AiUInt8 b_Chn, AiUInt16 w_Ftm);
AI_LIB_FUNC AiUInt8 AI_CALL_CONV Api429CmdTxXferDefEx(AiUInt8 b_Module, AiUInt8 b_Chn, TY_API429_LXFER_CMD *px_Xfer, TY_API429_LXFER_OUT *px_XferInfo);
AI_LIB_FUNC AiUInt8 AI_CALL_CONV Api429CmdTxXferDef(AiUInt8 b_Module, AiUInt8 b_Chn, TY_API429_LXFER_CMD *px_Xfer, AiUInt8 *b_Status, AiUInt32* pl_FreeMem);
AI_LIB_FUNC AiUInt8 AI_CALL_CONV Api429CmdTxXferDyntagDef(AiUInt8 b_Module, AiUInt8 b_Chn, AiUInt32 ul_XferId, TY_API429_LXFER_DYNTAG *px_Dyntag);
AI_LIB_FUNC AiUInt8 AI_CALL_CONV Api429CmdTxXferDataDef(AiUInt8 b_Module, AiUInt8 b_Chn, AiUInt32 ul_XferId, AiUInt8 b_XferMode,AiUInt16 w_BufStart, AiUInt16 w_BufSize, AiUInt32  *pl_LData);
AI_LIB_FUNC AiUInt8 AI_CALL_CONV Api429CmdTxXferDataRead(AiUInt8 b_Module, AiUInt8 b_Chn, TY_API429_XFER_DATA_READ_INPUT *px_XferDataInput,TY_API429_XFER_DATA *px_XferData);
AI_LIB_FUNC AiUInt8 AI_CALL_CONV Api429CmdTxMinFrameInstEx(AiUInt8 b_Module, AiUInt8 b_Chn, TY_API429_MFRAME_IN *px_MFrame, TY_API429_MFRAME_OUT *px_MFrameInfo);
AI_LIB_FUNC AiUInt8 AI_CALL_CONV Api429CmdTxMinFrameInst(AiUInt8 b_Module, AiUInt8 b_Chn, AiUInt32 ul_FrmId, AiUInt32 ul_XferCnt, AiUInt32 *pul_Xfers);
AI_LIB_FUNC AiUInt8 AI_CALL_CONV Api429CmdTxMajFrameInst(AiUInt8 b_Module, AiUInt8 b_Chn, AiUInt32 ul_FrmCnt, AiUInt32 *pul_Frames);
AI_LIB_FUNC AiUInt8 AI_CALL_CONV Api429CmdTxXferDel(AiUInt8 b_Module, AiUInt8 b_Chn, AiUInt32 ul_XferId);
AI_LIB_FUNC AiUInt8 AI_CALL_CONV Api429CmdTxXferEnaDis(AiUInt8 b_Module, AiUInt8 b_Chn, AiUInt8 b_Mode, AiUInt32 ul_XferId);
AI_LIB_FUNC AiUInt8 AI_CALL_CONV Api429CmdTxMinFrmDel(AiUInt8 b_Module, AiUInt8 b_Chn, AiUInt32 ul_FrmId);
AI_LIB_FUNC AiUInt8 AI_CALL_CONV Api429CmdTxMajFrmDel(AiUInt8 b_Module, AiUInt8 b_Chn);
AI_LIB_FUNC AiUInt8 AI_CALL_CONV Api429CmdTxXferReadEx(AiUInt8 b_Module, AiUInt8 b_Chn, AiUInt32 b_XferId, TY_API429_XFER_INFO  *px_XferInfo);
AI_LIB_FUNC AiUInt8 AI_CALL_CONV Api429CmdTxGetBufAddress(AiUInt8 b_Module, AiUInt8 b_Chn, AiUInt32 ul_XferId, AiUInt32 *pl_Addr, AiUInt16 *pw_Size);
AI_LIB_FUNC AiUInt8 AI_CALL_CONV Api429CmdTxHalt(AiUInt8 b_Module, AiUInt8 b_Chn);
AI_LIB_FUNC AiUInt8 AI_CALL_CONV Api429CmdRmTrgParamDef(AiUInt8 b_Module, AiUInt8 b_Chn, TY_API429_TRG_PAR_CMD *px_Para);
AI_LIB_FUNC AiUInt8 AI_CALL_CONV Api429CmdRmFuncBlkDef(AiUInt8 b_Module, AiUInt8 b_Chn, AiUInt8 b_Label, AiUInt8 b_Sdi, AiUInt8 b_Con, TY_API429_RCV_FB_CMD *px_FuncBlk);
AI_LIB_FUNC AiUInt8 AI_CALL_CONV Api429CmdTxFifoReset(AiUInt8 b_Module, AiUInt8 b_Chn);
AI_LIB_FUNC AiUInt8 AI_CALL_CONV Api429CmdTxFifoSetup(AiUInt8 b_Module, AiUInt8 b_Chn, TY_API429_TX_FIFO_SETUP *pxFifoSetup);
AI_LIB_FUNC AiUInt8 AI_CALL_CONV Api429CmdTxFifoStatus(AiUInt8 b_Module, AiUInt8 b_Chn, TY_API429_TX_FIFO_STATUS *pxFifoStatus);
AI_LIB_FUNC AiUInt8 AI_CALL_CONV Api429CmdTxFifoWriteLabels(AiUInt8 b_Module, AiUInt8 b_Chn, AiUInt32 ulMode, AiUInt32 ulCount, AiUInt32 *pulLabels,AiUInt32 *pulEntriesWritten);
AI_LIB_FUNC AiUInt8 AI_CALL_CONV Api429CmdTxFifoWriteInstructions(AiUInt8 b_Module, AiUInt8 b_Chn, AiUInt32 ulMode, AiUInt32 ulCount,TY_API429_TX_FIFO_ENTRY *pxFifoEntries, AiUInt32 *pulEntriesWritten);

/* API Memory Commands */
AI_LIB_FUNC AiUInt8 AI_CALL_CONV Api429CmdRamWrite(AiUInt8 b_Module, AiUInt8 b_WCount, AiUInt32 l_StartAddr, AiUInt32  *pl_WData);
AI_LIB_FUNC AiUInt8 AI_CALL_CONV Api429CmdRamRead(AiUInt8 b_Module, AiUInt8 b_WCount, AiUInt32 l_StartAddr, AiUInt32  *pl_WData);
AI_LIB_FUNC AiUInt8 AI_CALL_CONV Api429ReadBiuData(AiUInt8 bModule, AiUInt32 offset, AiUInt8 width, void* data_p);
AI_LIB_FUNC AiUInt8 AI_CALL_CONV Api429WriteBiuData(AiUInt8 bModule, AiUInt32 offset, AiUInt8 width, void* data_p);
AI_LIB_FUNC AiUInt8 AI_CALL_CONV Api429ReadBlockBiuData(AiUInt8 bModule, AiUInt32 offset, AiUInt8 width, void* data_p, AiUInt32 size);
AI_LIB_FUNC AiUInt8 AI_CALL_CONV Api429WriteBlockBiuData(AiUInt8 bModule, AiUInt32 offset, AiUInt8 width, void* data_p, AiUInt32 size);
AI_LIB_FUNC AiUInt8 AI_CALL_CONV Api429CmdMemAlloc(AiUInt8 bModule, AiUInt32 ulMemType, AiUInt32 ulSize, AiUInt32 ulTag, AiUInt32 *pulOffset);
AI_LIB_FUNC AiUInt8 AI_CALL_CONV Api429CmdMemFree(AiUInt8 b_Module, AiUInt32 ulMemType, AiUInt32 ulOffset);
AI_LIB_FUNC short AI_CALL_CONV Api429ReadBSPVersion(AiUInt8 bModule,
                                       AiUInt32 *pul_FirmwareVer, AiUInt32 *pul_TargetVer,
                                       AiUInt32 *pul_LcaVer1, AiUInt32 *pul_LcaVer2,
                                       AiUInt32 *pul_LcaCheckSum,
                                       AiUInt32 *pul_SysDrvVer, AiUInt32 *pul_DllVer,
                                       AiUInt32 *pul_BoardSerialNr,
                                       AiUInt8  *puc_BspCompatibility );
#endif /* API429_BSP10_LEGACY_H_ */
